using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

public class JwtQueryHandler : AuthorizationHandler<JwtQueryRequirement>
{
    private readonly IConfiguration _configuration;

    public JwtQueryHandler(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    protected override Task HandleRequirementAsync(
        AuthorizationHandlerContext context,
        JwtQueryRequirement requirement)
    {
        if (context.Resource is DefaultHttpContext httpContext)
        {
            // Check if the JWT token is present in the query string
            if (httpContext.Request.Query.TryGetValue("jwt", out var jwtValue))
            {
                // Validate the JWT token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]);

                try
                {
                    tokenHandler.ValidateToken(jwtValue, new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ClockSkew = TimeSpan.Zero,
                        ValidateLifetime = true
                    }, out _);

                    context.Succeed(requirement);
                }
                catch
                {
                    context.Fail();
                }
            }
        }

        return Task.CompletedTask;
    }
}
