﻿using Microsoft.EntityFrameworkCore;
using S2Api.Models;
using S2Api.Models.GameServers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S2Api.Repository
{
    public interface IServerRepository
    {
        Task<List<GameServerModel>> GetOnlineServerList();

    }
    public class ServerRepository : IServerRepository
    {
        private readonly MasterServerContext _context;
        public ServerRepository(MasterServerContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Returns a list of current online servers.
        /// </summary>
        /// <returns></returns>
        public async Task<List<GameServerModel>> GetOnlineServerList()
        {
            return await _context.Servers/*.Where(x => x.Online == true && x.Updated >= DateTime.Now.AddMinutes(-15))*/.Select(x => new GameServerModel()
            {
                Description = x.Description,
                Map = x.Map,
                Name = x.Name,
                NextMap = x.NextMap,
                Status = x.Status,
                Official = x.Official == "1", //yeah idk why it's a string in the database.
                NumConnected = x.NumConn.GetValueOrDefault(0),
                MaxConnected = x.MaxConn.GetValueOrDefault(0),
                Ip = $"{x.Ip}:{x.Port}",
            }).ToListAsync();
        }
    }
}
