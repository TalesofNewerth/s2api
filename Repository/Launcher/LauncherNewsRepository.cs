using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S2Api.Models;
using S2Api.Models.Launcher;

namespace S2Api.Repository
{
    public interface ILauncherNewsRepository
    {
        Task<IEnumerable<LauncherArticle>> GetArticlesAsync();
        Task<LauncherArticle> GetArticleByIdAsync(int id);
        Task AddArticleAsync(LauncherArticle article);
        Task UpdateArticleAsync(LauncherArticle article);
        Task DeleteArticleAsync(int id);
    }

    public class LauncherNewsRepository : ILauncherNewsRepository
    {
        private readonly MasterServerContext _context;

        public LauncherNewsRepository(MasterServerContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<LauncherArticle>> GetArticlesAsync()
        {
            return await _context.LauncherArticles.ToListAsync();
        }

        public async Task<LauncherArticle> GetArticleByIdAsync(int id)
        {
            return await _context.LauncherArticles.FindAsync(id);
        }

        public async Task AddArticleAsync(LauncherArticle article)
        {
            _context.LauncherArticles.Add(article);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateArticleAsync(LauncherArticle article)
        {
            _context.Entry(article).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteArticleAsync(int id)
        {
            var article = await _context.LauncherArticles.FindAsync(id);
            if (article != null)
            {
                _context.LauncherArticles.Remove(article);
                await _context.SaveChangesAsync();
            }
        }
    }
    public interface IArticleContentRepository
    {
        Task<LauncherArticleContent> GetContentByMdAsync(string md);
        Task CreateArticleContentAsync(LauncherArticleContent articleContent);
        Task UpdateArticleContentAsync(LauncherArticleContent articleContent);
        Task DeleteArticleContentAsync(string md);
    }
    public class ArticleContentRepository : IArticleContentRepository
    {
        private readonly MasterServerContext _context;

        public ArticleContentRepository(MasterServerContext context)
        {
            _context = context;
        }

        public async Task<LauncherArticleContent> GetContentByMdAsync(string md)
        {
            return await _context.LauncherArticleContent.FirstOrDefaultAsync(ac => ac.Md == md);
        }

        public async Task CreateArticleContentAsync(LauncherArticleContent articleContent)
        {
            _context.LauncherArticleContent.Add(articleContent);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateArticleContentAsync(LauncherArticleContent articleContent)
        {
            _context.LauncherArticleContent.Update(articleContent);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteArticleContentAsync(string md)
        {
            var articleContent = await GetContentByMdAsync(md);
            if (articleContent != null)
            {
                _context.LauncherArticleContent.Remove(articleContent);
                await _context.SaveChangesAsync();
            }
        }
    }

}