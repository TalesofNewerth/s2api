using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S2Api.Models;
using S2Api.Models.Launcher;

namespace S2Api.Repository
{
    public interface ILauncherUpdateRepository
    {
        Task<LauncherUpdate> GetLatestAsync();
        Task<LauncherUpdate> GetByIdAsync(int id);
        Task<LauncherUpdate> AddAsync(LauncherUpdate launcherUpdate);
        Task<LauncherUpdate> UpdateAsync(LauncherUpdate launcherUpdate);
        Task DeleteAsync(int id);
    }

    public class LauncherUpdateRepository : ILauncherUpdateRepository
    {
        private readonly MasterServerContext _context;

        public LauncherUpdateRepository(MasterServerContext context)
        {
            _context = context;
        }

        public async Task<LauncherUpdate> GetLatestAsync() 
        {
            return await _context.LauncherUpdates
                    .OrderByDescending(r => r.Version)
                    .ThenByDescending(r => r.PubDate)
                    .FirstOrDefaultAsync();
        }

        public async Task<LauncherUpdate> GetByIdAsync(int id)
        {
            return await _context.LauncherUpdates.FindAsync(id);
        }

        public async Task<LauncherUpdate> AddAsync(LauncherUpdate launcherUpdate)
        {
            _context.LauncherUpdates.Add(launcherUpdate);
            await _context.SaveChangesAsync();
            return launcherUpdate;
        }

        public async Task<LauncherUpdate> UpdateAsync(LauncherUpdate launcherUpdate)
        {
            _context.Entry(launcherUpdate).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return launcherUpdate;
        }

        public async Task DeleteAsync(int id)
        {
            var launcherUpdate = await _context.LauncherUpdates.FindAsync(id);
            if (launcherUpdate != null)
            {
                _context.LauncherUpdates.Remove(launcherUpdate);
                await _context.SaveChangesAsync();
            }
        }
    }
}
