using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S2Api.Models;
using S2Api.Models.MasterServer;
using S2Api.Models.Authentication;
using System.Linq;
using System.Collections.Generic;

namespace S2Api.Repository
{
    public interface IReleasesRepository
    {
        Task<IEnumerable<Release>> GetAllReleasesAsync();
        Task<Release> GetReleaseAsync(int id);
        Task<Release> GetReleaseAsync(string clientName, string tagName);
        Task<Release> GetLatestReleaseAsync(string clientName);
        Task<Release> AddReleaseAsync(Release release);
        Task UpdateReleaseAsync(Release release);
        Task DeleteReleaseAsync(int id);
    }

    public class ReleasesRepository : IReleasesRepository
    {
        private readonly MasterServerContext _context;

        public ReleasesRepository(MasterServerContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Release>> GetAllReleasesAsync()
        {
            return await _context.Releases.Include(r => r.Assets).ToListAsync();
        }

        public async Task<Release> GetReleaseAsync(int id)
        {
            return await _context.Releases.Include(r => r.Assets).FirstOrDefaultAsync(r => r.Id == id);
        }

        public async Task<Release> GetReleaseAsync(string clientName, string tagName)
        {
            return await _context.Releases.Include(r => r.Assets)
                .FirstOrDefaultAsync(r => r.Name == clientName && r.TagName == tagName);
        }

        public async Task<Release> GetLatestReleaseAsync(string clientName)
        {
            return await _context.Releases.Include(r => r.Assets)
                .Where(r => r.Name == clientName)
                .OrderByDescending(r => r.PublishedAt)
                .FirstOrDefaultAsync();
        }

        public async Task<Release> AddReleaseAsync(Release release)
        {
            _context.Releases.Add(release);
            await _context.SaveChangesAsync();
            return release;
        }

        public async Task UpdateReleaseAsync(Release release)
        {
            _context.Entry(release).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteReleaseAsync(int id)
        {
            var release = await _context.Releases.FindAsync(id);
            if (release != null)
            {
                _context.Releases.Remove(release);
                await _context.SaveChangesAsync();
            }
        }
    }
}
