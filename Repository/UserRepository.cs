using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using S2Api.Models;
using S2Api.Models.MasterServer;
using S2Api.Models.Authentication;
using System.Linq;

namespace S2Api.Repository
{
    public interface IUserRepository
    {
        Task<User> GetUserByUsername(string username);
        Task<User> GetUserByEmail(string email);
        Task<User> GetUserById(int id);
        Task Register(User user);
        Task CreateEmailConfirmationToken(User user, string token);
        Task<EmailConfirmationToken> GetEmailConfirmationToken(string token);
        Task SetLastVerificationEmailSent(User user);
        Task UpdateRefreshTokens(User user, RefreshToken token);
        Task<RefreshToken> GetRefreshToken(string token);
        Task ConfirmEmail(User user);
        Task<string> GeneratePasswordResetTokenAsync(User user);
        Task ResetPasswordAsync(User user);
        Task<bool> IsUserValidated(string username, string password, int? accountType = null);
        Task<bool> DoesEmailExist(string email);
    }

    public class UserRepository : IUserRepository
    {
        private readonly MasterServerContext _context;

        public UserRepository(MasterServerContext context)
        {
            _context = context;
        }

        public async Task<User> GetUserByUsername(string username)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Username == username);
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
        }

        public async Task Register(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
        }

        public async Task CreateEmailConfirmationToken(User user, string token)
        {
            var confirmationToken = new EmailConfirmationToken
            {
                UserId = user.Id,
                Token = token,
                CreatedAt = DateTime.UtcNow,
                ExpirationDate = DateTime.UtcNow.AddDays(1)
            };
            _context.EmailConfirmationTokens.Add(confirmationToken);
            await _context.SaveChangesAsync();
        }

        public async Task<EmailConfirmationToken> GetEmailConfirmationToken(string token)
        {
            return await _context.EmailConfirmationTokens.FirstOrDefaultAsync(t => t.Token == token);
        }

        public async Task<User> GetUserById(int id)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task SetLastVerificationEmailSent(User user)
        {
            user.LastVerificationEmailSentAt = DateTime.UtcNow;
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateRefreshTokens(User user, RefreshToken token)
        {
            var tokensToRemove = _context.RefreshTokens
                .Where(rt => rt.User.Id == user.Id && (rt.Revoked || rt.ExpirationDate < DateTime.UtcNow))
                .ToList();

            foreach (var tokenToRemove in tokensToRemove)
            {
                _context.RefreshTokens.Remove(tokenToRemove);
            }

            user.RefreshTokens.Add(token);
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task<RefreshToken> GetRefreshToken(string token)
        {
            return await _context.RefreshTokens.FirstOrDefaultAsync(rt => rt.Token == token);
        }

        public async Task ConfirmEmail(User user)
        {
            var tokensToRemove = _context.EmailConfirmationTokens
                .Where(rt => rt.UserId == user.Id)
                .ToList();

            foreach (var tokenToRemove in tokensToRemove)
            {
                user.EmailConfirmationTokens.Remove(tokenToRemove);
            }

            user.EmailConfirmed = true;
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task<string> GeneratePasswordResetTokenAsync(User user)
        {
            var token = Guid.NewGuid().ToString();
            user.ResetPasswordToken = token;
            user.ResetPasswordExpiration = DateTime.UtcNow.AddHours(1);
            await _context.SaveChangesAsync();

            return token;
        }

        public async Task ResetPasswordAsync(User user)
        {
            user.ResetPasswordToken = null;
            user.ResetPasswordExpiration = null;
            await _context.SaveChangesAsync();
        }

        public async Task<bool> IsUserValidated(string username, string password, int? accountType = null)
        {
            var user = await GetUserByUsername(username);

            if (accountType != null && user.Permissions != accountType)
            {
                return false;
            }

            return user != null && BCrypt.Net.BCrypt.Verify(password, user.Password);
        }
        
        public async Task<bool> DoesEmailExist(string email)
        {
            return await _context.Users.AnyAsync(u => u.Email == email);
        }
    }
}
