namespace S2Api.Interfaces;

public interface IError {
    string Message { get; set; }
}