namespace S2Api.Interfaces;

public interface IValidationError : IError {
    object Data { get; set; }
}
public class ValidationError : IValidationError {
    public string Message { get; set; }
    public object Data { get; set; }

    public ValidationError(string message) {
        Message = message;
    }

    public ValidationError(string message, object data) {
        Message = message;
        Data = data;
    }
}