using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S2Api.Services;
using System;
using System.Collections.Concurrent;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace S2Api.Controllers
{
    [Authorize(Policy = "JwtQuery")]
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly IEventService _eventService;

        public EventsController(IEventService eventService)
        {
            _eventService = eventService;
        }

        [HttpGet("updates")]
        public async Task<IActionResult> Connect([FromQuery] string jwt)
        {
            if (string.IsNullOrEmpty(jwt))
            {
                return BadRequest();
            }

            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(jwt);

            var accountId = token.Claims.FirstOrDefault(c => c.Type == "accountId")?.Value;

            var response = Response.HttpContext.Response;
            response.Headers.Add("Content-Type", "text/event-stream");
            response.Headers.Add("Cache-Control", "no-cache");
            response.Headers.Add("Connection", "keep-alive");
            response.Headers.Add("Access-Control-Allow-Origin", "*");

            if (string.IsNullOrWhiteSpace(accountId))
                return Forbid();

            if (!HttpContext.Request.Headers.TryGetValue("Accept", out var acceptHeader) ||
                !acceptHeader.Any(header => header.Equals("text/event-stream", StringComparison.OrdinalIgnoreCase)))
            {
                return BadRequest();
            }

            if (HttpContext.Response.Body is Stream responseStream)
            {
                var eventStreamWriter = new EventStreamWriter(responseStream);
                _eventService.AddEventStreamWriter(int.Parse(accountId), eventStreamWriter);

                try
                {
                    await eventStreamWriter.WriteAsync(": connected\n\n");
                    await eventStreamWriter.FlushAsync();

                    await Task.Delay(-1, HttpContext.RequestAborted);
                }
                finally
                {
                    _eventService.RemoveEventStreamWriter(int.Parse(accountId));
                    eventStreamWriter.Dispose();
                }
            }

            return Ok();
        }
    }
}
