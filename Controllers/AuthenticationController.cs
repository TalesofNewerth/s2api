// using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using S2Api.Models.Authentication;
using S2Api.Requests;
using S2Api.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace S2Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IEmailService _emailService;
        private readonly IRefreshTokenService _tokenService;
        private readonly IConfiguration _configuration;

        public AuthenticationController(IAuthenticationService authenticationService, IEmailService emailService, IRefreshTokenService tokenService, IConfiguration configuration)
        {
            _authenticationService = authenticationService;
            _emailService = emailService;
            _tokenService = tokenService;
            _configuration = configuration;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync(RegisterRequest request)
        {
            var exists = await _authenticationService.CheckIfExists(request.Username, request.Email);

            if (exists)
            {
                return Conflict("Username or Email is already taken.");
            }

            var newUser = await _authenticationService.Register(request.Username, request.Email, request.Password);

            var confirmationToken = await _authenticationService.GenerateEmailConfirmationTokenAsync(newUser);

            var encodedToken = System.Web.HttpUtility.UrlEncode(confirmationToken);
            var clientBaseUrl = _configuration["ClientBaseUrl"];
            var callbackUrl = $"{clientBaseUrl}/verify-email?token={encodedToken}";

            var message = $"<h1>Welcome, {newUser.Username}!</h1><p>Please validate your e-mail address by clicking the link below to complete account creation.</p><a href='{callbackUrl}' class='button'>Verify Email</a>";
            await _emailService.SendEmailAsync(
                newUser.Email,
                $"Welcome to Savage 2, {newUser.Username}!",
                message
            );

            return Ok("Registration Successful!");
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync(LoginRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _authenticationService.GetUserByEmail(request.Email);
            if (user == null)
            {
                return Unauthorized();
            }

            if (!user.EmailConfirmed)
            {
                await this.ResendVerificationEmailAsync(new ResendVerificationRequest() { Email = request.Email });

                return BadRequest(new { message = "Please confirm your email address before logging in.", code = "EMAIL_NOT_VERIFIED" });
            }

            var passwordValid = this._authenticationService.VerifyPassword(request.Password, user.Password);
            if (passwordValid == false)
            {
                return Unauthorized();
            }

            var jwt = await _authenticationService.Authenticate(user);

            var refreshToken = new RefreshToken
            {
                Token = _tokenService.GenerateRefreshToken(),
                ExpirationDate = DateTime.UtcNow.AddDays(7),
                CreatedAt = DateTime.UtcNow,
                Revoked = false,
                User = user,
                UserId = user.Id
            };

            await _authenticationService.UpdateRefreshTokens(user, refreshToken);

            return Ok(new { Token = jwt, RefreshToken = refreshToken.Token });
        }


        [AllowAnonymous]
        [HttpPost("check-exists")]
        public async Task<IActionResult> CheckExistsAsync([FromBody] CheckExistsRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var exists = await _authenticationService.CheckIfExists(request.Username, null);

            return Ok(new { usernameExists = exists });
        }
        
        [AllowAnonymous]
        [HttpPost("verify-email")]
        public async Task<IActionResult> VerifyEmailAsync(IfExistEmailRequest request)
        {
            var exists = await _authenticationService.VerifyEmailIfExist(request.Email);

            return Ok(exists);
        }

        [AllowAnonymous]
        [HttpGet("confirm-email")]
        public async Task<IActionResult> ConfirmEmail([FromQuery] string token)
        {
            var confirmationToken = await _authenticationService.GetEmailConfirmationToken(token);
            if (confirmationToken == null)
                return NotFound();

            var user = await _authenticationService.GetUserById(confirmationToken.UserId);
            if (user == null)
                return NotFound();

            await _authenticationService.ConfirmEmail(user);
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("resend-verification")]
        public async Task<IActionResult> ResendVerificationEmailAsync([FromBody] ResendVerificationRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _authenticationService.GetUserByEmail(request.Email);
            if (user == null)
            {
                return NotFound("User with this email does not exist.");
            }

            if (user.EmailConfirmed)
            {
                return Conflict("This email address has already been verified.");
            }

            if (user.LastVerificationEmailSentAt.HasValue && (DateTime.UtcNow - user.LastVerificationEmailSentAt.Value).TotalMinutes < 15)
            {
                return Conflict("Please wait a few minutes before requesting another verification email.");
            }

            var confirmationToken = await _authenticationService.GenerateEmailConfirmationTokenAsync(user);

            var encodedToken = System.Web.HttpUtility.UrlEncode(confirmationToken);
            var clientBaseUrl = _configuration["ClientBaseUrl"];
            var callbackUrl = $"{clientBaseUrl}/verify-email?token={encodedToken}";

            var message = $"<h1>Welcome, {user.Username}!</h1><p>Please validate your e-mail address by clicking the link below to complete account creation.</p><a href='{callbackUrl}' class='button'>Verify Email</a>";
            await _emailService.SendEmailAsync(
                user.Email,
                $"Welcome to Savage 2, {user.Username}!",
                message
            );

            await _authenticationService.SetLastVerificationEmailSent(user);

            return Ok("Verification email sent!");
        }

        [HttpPost("refresh-token")]
        public async Task<IActionResult> RefreshToken(RefreshTokenRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (string.IsNullOrWhiteSpace(request.RefreshToken))
                return BadRequest(ModelState);

            var refreshToken = await _authenticationService.GetRefreshToken(request.RefreshToken);
            if (refreshToken == null || refreshToken.Revoked || refreshToken.ExpirationDate < DateTime.UtcNow)
            {
                return Unauthorized();
            }

            var jwt = await _authenticationService.RefreshJwtToken(refreshToken);

            var newRefreshToken = new RefreshToken
            {
                Token = _tokenService.GenerateRefreshToken(),
                ExpirationDate = DateTime.UtcNow.AddDays(7),
                CreatedAt = DateTime.UtcNow,
                Revoked = false
            };

            await _authenticationService.UpdateRefreshTokens(refreshToken.User, newRefreshToken);

            return Ok(new { Token = jwt, RefreshToken = newRefreshToken.Token });
        }

        [AllowAnonymous]
        [HttpPost("forgot-password")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordRequest model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _authenticationService.GetUserByEmail(model.Email);
            if (user == null)
                return BadRequest("Invalid request");

            var token = await _authenticationService.GeneratePasswordResetTokenAsync(user);

            var encodedToken = System.Web.HttpUtility.UrlEncode(token);
            var clientBaseUrl = _configuration["ClientBaseUrl"];
            var callbackUrl = $"{clientBaseUrl}/reset-password?token={encodedToken}&email={user.Email}";

            var message = $"<h1>Password reset requested for <b>{user.Username}</b>.</h1><p>Click the link below to change your password. If you did not request a password reset, you may safely ignore this email.</p><a href='{callbackUrl}' class='button'>Reset Password</a><br /><br />";

            await _emailService.SendEmailAsync(
                model.Email,
                "Reset Password Request",
                message);


            return Ok("A password reset link has been sent to your email.");
        }

        [AllowAnonymous]
        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword(ResetPasswordRequest model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = await _authenticationService.GetUserByEmail(model.Email);
            if (user == null)
                return BadRequest("Invalid request");

            var result = await _authenticationService.ResetPasswordAsync(user, model.Token, model.Password);
            if (result.Succeeded)
                return Ok("Your password has been reset.");

            return BadRequest("Password reset failed.");
        }
    }

    public class LoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class CheckExistsRequest
    {
        public string Username { get; set; }
    }

    public class ResendVerificationRequest
    {
        public string Email { get; set; }
    }

    public class RefreshTokenRequest
    {
        public string RefreshToken { get; set; }
    }

    public class ForgotPasswordRequest
    {
        public string Email { get; set; }
    }

    public class ResetPasswordRequest
    {
        public string Token { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
