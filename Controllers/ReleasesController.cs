using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using S2Api.Models;
using S2Api.Repository;

namespace S2Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ReleasesController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IReleasesRepository _releasesRepository;

        public ReleasesController(IConfiguration configuration, IReleasesRepository releasesRepository)
        {
            _releasesRepository = releasesRepository;
            _configuration = configuration;
        }

        [HttpGet("{clientName}/{version}")]
        public async Task<IActionResult> GetRelease(string clientName, string version)
        {
            var release = await _releasesRepository.GetReleaseAsync(clientName, version);
            if (release == null)
            {
                return NotFound();
            }

            var response = CreateReleaseResponse(release);
            return Ok(response);
        }

        [HttpGet("{clientName}/latest")]
        public async Task<IActionResult> GetLatestRelease(string clientName)
        {
            var latestRelease = await _releasesRepository.GetLatestReleaseAsync(clientName);
            if (latestRelease == null)
            {
                return NotFound();
            }

            var response = CreateReleaseResponse(latestRelease);
            return Ok(response);
        }

        [Authorize(Policy = "Admin")]
        [HttpPost]
        public async Task<IActionResult> CreateRelease([FromBody] Release release)
        {
            if (await _releasesRepository.GetReleaseAsync(release.Name, release.TagName) != null)
            {
                return Conflict(new { message = "Release with the same version already exists." });
            }

            var currentTime = DateTime.UtcNow;
            release.CreatedAt ??= currentTime;
            release.PublishedAt ??= currentTime;
            
            var createdRelease = await _releasesRepository.AddReleaseAsync(release);
            var response = CreateReleaseResponse(createdRelease);
            return CreatedAtAction(nameof(GetRelease), new { clientName = createdRelease.Name, version = createdRelease.TagName }, response);
        }

        private object CreateReleaseResponse(Release release)
        {
            var baseUrl = _configuration["ClientBaseUrl"];
            
            return new
            {
                url = $"{baseUrl}/api/releases/{release.Id}",
                assets_url = $"{baseUrl}/api/releases/{release.Id}/assets",
                html_url = $"{baseUrl}/releases/{release.TagName}",
                id = release.Id,
                tag_name = release.TagName,
                name = release.Name,
                draft = release.IsDraft,
                prerelease = release.IsPrerelease,
                created_at = release.CreatedAt,
                published_at = release.PublishedAt,
                assets = release.Assets.Select(a => new
                {
                    url = $"{baseUrl}/api/assets/{a.Id}",
                    id = a.Id,
                    name = a.Name,
                    content_type = a.ContentType,
                    size = a.Size,
                    download_url = a.DownloadUrl
                })
            };
        }
    }
}
