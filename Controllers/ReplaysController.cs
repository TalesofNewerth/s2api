﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Minio;
using Minio.AspNetCore;
using S2Api.Models;
using S2Api.Requests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace S2Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReplaysController : ControllerBase
    {
        private readonly MasterServerContext _context;
        private readonly IConfiguration _config;
        private readonly MinioClient _minio;
        public ReplaysController(MasterServerContext context, IConfiguration config, IMinioClientFactory minio)
        {
            _context = context;
            _config = config;
            _minio = minio.CreateClient("ovh");
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<string>), 200)]
        [Route("")]
        public IActionResult Index()
        {
            var request = new ListObjectsArgs().WithBucket(_config["ReplayBucket"]).WithRecursive(true);

            var result = _minio.ListObjectsAsync(request);
            List<string> items = new();
            var subscription = result.Subscribe(
                item => items.Add(item.Key),
                ex => Console.WriteLine("OnError: {0}", ex.Message),
                () => Console.WriteLine("OnComplete: {0}")
            );

            return Ok(items);
        }

        [HttpGet]
        [Route("{matchId:required:int}")]
        [ProducesResponseType(typeof(FileContentResult), 200)]
        public async Task<IActionResult> GetFile(int matchId)
        {
            IActionResult returnValue = NotFound();
            var file = new MemoryStream();

            var fileName = $"{matchId}.s2r";

            var getObject = new GetObjectArgs().WithBucket(_config["ReplayBucket"]).WithObject(fileName).WithCallbackStream((stream) => {
                stream.CopyTo(file);
            });

            await _minio.GetObjectAsync(getObject);

            return File(file.ToArray(), "application/octet-stream", fileName);
        }

        [HttpPost]
        [Route("upload/{matchId:required:int}")]
        [RequestSizeLimit(100_000_000)]
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 401)]
        public async Task<IActionResult> UploadReplay([FromForm] SubmitReplayRequest request, int matchId)
        {
            if(matchId == 0 || string.IsNullOrWhiteSpace(request.Username) || string.IsNullOrWhiteSpace(request.Password))
            {
                return BadRequest();
            }

            var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == request.Username);

            if(user == null || !BCrypt.Net.BCrypt.Verify(request.Password, user.Password))
            {
                return Unauthorized();
            }

            if(!await _context.Matches.AnyAsync(x => x.Id == matchId))
            {
                return BadRequest($"This match {matchId} does not exist");
            }

            var awsRequest = new PutObjectArgs().WithBucket(_config["ReplayBucket"]).WithObject($"{matchId}.s2r").WithContentType("application/octet-stream").WithObjectSize(request.File.Length).WithStreamData(request.File.OpenReadStream());

            await _minio.PutObjectAsync(awsRequest);

            return Ok("Replay accepted");
        }
    }
}