﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using S2Api.Models;
using S2Api.Models.GameServers;
using S2Api.Repository;
using S2Api.Services;

namespace S2Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServerController : ControllerBase
    {
        private readonly MasterServerContext _context;
        private readonly IServerRepository _serverRepository;
        public ServerController(MasterServerContext context, IServerRepository serverRepository)
        {
            _context = context;
            _serverRepository = serverRepository;
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<GameServerModel>), 200)]
        public async Task<IActionResult> Index()
        {
            return Ok(await _serverRepository.GetOnlineServerList());
        }

        [HttpGet]
        [Route("status/{id:required}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(bool), 400)]
        public async Task<IActionResult> GetStatus(int id)
        {
            var server = await _context.Servers.FindAsync(id);

            return server != null && server.Online.GetValueOrDefault(false) && server.Updated >= DateTime.UtcNow.AddMinutes(-15) ? Ok(true) : BadRequest(false);
        }
    }
}