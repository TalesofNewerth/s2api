using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using S2Api.Enums;
using S2Api.Models;
using S2Api.Models.MasterServer;
using S2Api.Requests.Items;
using S2Api.Services;
using Serilog;

namespace S2Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly IItemService _itemService;

        public ItemsController(IItemService itemService)
        {
            _itemService = itemService;
        }

        [HttpGet]
        public IActionResult GetAffixes()
        {
            return Ok(_itemService.GetAffixes());
        }

        [HttpGet("active")]
        public async Task<IActionResult> GetActiveItems()
        {
            try
            {
                var result = await _itemService.GetActiveItems();
                return result.Match<IActionResult>(
                    Ok,
                    validationError => BadRequest(validationError.Message));
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
                // Handle the exception or return an error response
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving items.");
            }
        }

        [HttpGet("stored")]
        public async Task<IActionResult> GetStoredItems()
        {
            try
            {
                var result = await _itemService.GetStoredItems();
                return result.Match<IActionResult>(
                    Ok,
                    validationError => BadRequest(validationError.Message));
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving items.");
            }
        }

        [HttpPost("generate")]
        public async Task<IActionResult> GenerateNewItem([FromBody]CreateItemRequest request)
        {
            try
            {
                var result = await _itemService.CreateItem(request);
                return result.Match<IActionResult>(
                    success => Ok(success),
                    validationError => BadRequest(validationError.Message));
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while generating a new item.");
            }
        }
        
        [HttpPost("salvage/{itemId:int:required}")]
        public async Task<IActionResult> SalvageItem([FromRoute] int itemId)
        {
            try
            {
                var result = await _itemService.SalvageItem(itemId);
                return result.Match<IActionResult>(
                    success => Ok(success),
                    validationError => BadRequest(validationError.Message));
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while salvaging the item.");
            }
        }

        [HttpPost("activate/{itemId:int:required}")]
        public async Task<IActionResult> ActivateItem([FromRoute] int itemId)
        {
            try
            {
                var result = await _itemService.ActivateItem(itemId);
                return result.Match<IActionResult>(success => Ok(success), validationError => BadRequest(validationError.Message));
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while moving the item.");
            }
        }

        [HttpPost("delete-active/{itemId:int:required}")]
        public async Task<IActionResult> DeleteActiveItem([FromRoute] int itemId)
        {
            try
            {
                var result = await _itemService.DeleteActiveItem(itemId);
                return result.Match<IActionResult>(
                    success => Ok(success),
                    validationError => BadRequest(validationError.Message));
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while deleting the item.");
            }
        }

        [HttpGet("gold")]
        public async Task<IActionResult> GetGold()
        {
            try
            {
                var result = await _itemService.GetGold();
                return result.Match<IActionResult>(
                    gold => Ok(gold),
                    validationError => BadRequest(validationError.Message));
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while modifying the item.");
            }
        }
    }
}