#if DEBUG
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;

namespace S2Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileDownloadController : ControllerBase
    {
        private readonly ILogger<FileDownloadController> _logger;
        private readonly string _filePath = Path.Combine(Directory.GetCurrentDirectory(), "TestFiles");

        public FileDownloadController(ILogger<FileDownloadController> logger)
        {
            _logger = logger;
        }

        [HttpGet("{fileName}")]
        public IActionResult DownloadFile(string fileName)
        {
            var fileFullPath = Path.Combine(_filePath, fileName);

            if (!System.IO.File.Exists(fileFullPath))
            {
                _logger.LogError($"File not found: {fileFullPath}");
                return NotFound();
            }

            var contentType = "application/octet-stream";
            var fileBytes = System.IO.File.ReadAllBytes(fileFullPath);

            return File(fileBytes, contentType, fileName);
        }
    }
}
#endif