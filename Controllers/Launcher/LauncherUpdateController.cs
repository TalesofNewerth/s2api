using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using S2Api.Models.Launcher;
using S2Api.Repository;

namespace S2Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LauncherUpdatesController : ControllerBase
    {
        private readonly ILauncherUpdateRepository _repository;

        public LauncherUpdatesController(ILauncherUpdateRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("latest")]
        public async Task<ActionResult<LauncherUpdate>> GetLatestUpdate()
        {
            var latestUpdate = await _repository.GetLatestAsync();

            if (latestUpdate == null)
            {
                return NotFound();
            }

            return Ok(latestUpdate);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<LauncherUpdate>> GetById(int id)
        {
            var update = await _repository.GetByIdAsync(id);
            if (update == null)
            {
                return NotFound();
            }
            return Ok(update);
        }

        [Authorize(Policy = "Admin")]
        [HttpPost]
        public async Task<ActionResult<LauncherUpdate>> Create(LauncherUpdate launcherUpdate)
        {
            var createdUpdate = await _repository.AddAsync(launcherUpdate);
            return CreatedAtAction(nameof(GetById), new { id = createdUpdate.Id }, createdUpdate);
        }

        [Authorize(Policy = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, LauncherUpdate launcherUpdate)
        {
            if (id != launcherUpdate.Id)
            {
                return BadRequest();
            }

            await _repository.UpdateAsync(launcherUpdate);
            return NoContent();
        }

        [Authorize(Policy = "Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _repository.DeleteAsync(id);
            return NoContent();
        }
    }
}