using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using S2Api.Models.Launcher;
using S2Api.Repository;

[ApiController]
[Route("api/[controller]")]
public class LauncherNewsController : ControllerBase
{
    private readonly ILauncherNewsRepository _newsRepository;
    private readonly IArticleContentRepository _articleContentRepository;

    public LauncherNewsController(ILauncherNewsRepository newsRepository, IArticleContentRepository articleContentRepository)
    {
        _newsRepository = newsRepository;
        _articleContentRepository = articleContentRepository;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<LauncherArticle>>> GetArticles()
    {
        var articles = await _newsRepository.GetArticlesAsync();
        return Ok(new { articles });
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<LauncherArticle>> GetArticle(int id)
    {
        var article = await _newsRepository.GetArticleByIdAsync(id);
        if (article == null)
        {
            return NotFound();
        }
        return Ok(article);
    }

    [Authorize(Policy = "Admin")]
    [HttpPost]
    public async Task<ActionResult> CreateArticle(LauncherArticle article)
    {
        await _newsRepository.AddArticleAsync(article);
        return CreatedAtAction(nameof(GetArticle), new { id = article.Id }, article);
    }

    [Authorize(Policy = "Admin")]
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateArticle(int id, LauncherArticle article)
    {
        if (id != article.Id)
        {
            return BadRequest();
        }

        await _newsRepository.UpdateArticleAsync(article);
        return NoContent();
    }

    [Authorize(Policy = "Admin")]
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteArticle(int id)
    {
        await _newsRepository.DeleteArticleAsync(id);
        return NoContent();
    }

    [HttpGet("content/{md}")]
    public async Task<IActionResult> GetContentByMd(string md)
    {
        var content = await _articleContentRepository.GetContentByMdAsync(md);
        if (content == null)
        {
            return NotFound();
        }

        return Content(content.Content, "text/markdown");
    }

    [Authorize(Policy = "Admin")]
    [HttpPost("content")]
    public async Task<IActionResult> CreateArticleContent([FromBody] LauncherArticleContent articleContent)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        await _articleContentRepository.CreateArticleContentAsync(articleContent);
        return CreatedAtAction(nameof(GetContentByMd), new { md = articleContent.Md }, articleContent);
    }

    [Authorize(Policy = "Admin")]
    [HttpPut("content/{md}")]
    public async Task<IActionResult> UpdateArticleContent(string md, [FromBody] LauncherArticleContent articleContent)
    {
        if (!ModelState.IsValid || articleContent.Md != md)
        {
            return BadRequest(ModelState);
        }

        await _articleContentRepository.UpdateArticleContentAsync(articleContent);
        return NoContent();
    }

    [Authorize(Policy = "Admin")]
    [HttpDelete("content/{md}")]
    public async Task<IActionResult> DeleteArticleContent(string md)
    {
        await _articleContentRepository.DeleteArticleContentAsync(md);
        return NoContent();
    }
}
