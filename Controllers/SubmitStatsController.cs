﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using S2Api.Services;
using Serilog;

namespace S2Api.Controllers
{
    [Route("api/submit-stats")]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class SubmitStatsController : ControllerBase
    {
        private readonly ISubmitStatsService _submitStatsService;
        public SubmitStatsController(ISubmitStatsService submitStatsService) { 
            _submitStatsService = submitStatsService;
        }

        [HttpPost]
        [Route("")]
        [Consumes("application/x-www-form-urlencoded")]
        public async Task<IActionResult> SubmitMatch([FromForm]IFormCollection form)
        {
            try
            {
                if (!int.TryParse(form["svr_id"], out var svrId))
                {
                    return BadRequest("Invalid server id");
                }

                if (!int.TryParse(form["match_id"], out var matchId))
                {
                    return BadRequest("Invalid match id");
                }

                if (!int.TryParse(form["winner"], out var winner))
                {
                    return BadRequest("Invalid winner");
                }

                if (!TimeOnly.TryParse(form["time"], out var time))
                {
                    return BadRequest("Invalid timestamp");
                }
                
                var statsSent = await _submitStatsService.SubmitStats(form["login"], form["pass"], svrId, matchId, form["map"], winner, form["player_stats"], form["commander_stats"], time, form["team"]);

                return statsSent ? Ok("Replay uploaded") : BadRequest("Invalid request");
            }
            catch (Exception e)
            {
                Log.Error(e, e.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving news items.");
            }
        }
    }
}
