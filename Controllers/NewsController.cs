using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using S2Api.Models;
using S2Api.Services;

namespace S2Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly MasterServerContext _context;

        public NewsController(MasterServerContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllNewsItems([FromQuery] int page = 1, [FromQuery] int pageSize = 10, [FromQuery] string sortBy = "createdAt", [FromQuery] bool isAscending = false)
        {
            try
            {
                var articles = await _context.Articles
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize).ToListAsync();

                switch (sortBy.ToLower())
                {
                    case "createdAt":
                    default:
                        articles = isAscending ? articles.OrderBy(p => p.CreatedAt).ToList() : articles.OrderByDescending(p => p.CreatedAt).ToList();
                        break;
                }

                return Ok(articles);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving news items.");
            }
        }
    }
}