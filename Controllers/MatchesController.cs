using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S2Api.Services;

namespace S2Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchesController : ControllerBase
    {
        private readonly IStatsService _statsService;

        public MatchesController(IStatsService statsService)
        {
            _statsService = statsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllMatches([FromQuery] int page, [FromQuery] int pageSize, [FromQuery] string sortBy, [FromQuery] bool isAscending, [FromQuery] string filter)
        {
            try
            {
                var matches = await _statsService.GetAllMatchesAsync(page, pageSize, sortBy, isAscending, filter);

                return Ok(matches);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving matches.");
            }
        }

        [HttpGet("{matchId}")]
        public async Task<IActionResult> GetMatchById(int matchId)
        {
            try
            {
                var match = await _statsService.GetMatchByIdAsync(matchId);

                if (match == null)
                {
                    return NotFound();
                }

                return Ok(match);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving the match.");
            }
        }

        [HttpGet("stats/{matchId}")]
        public async Task<IActionResult> GetMatchStatsById(int matchId)
        {
            try
            {
                var matchStats = await _statsService.GetMatchStatsByIdAsync(matchId);

                if (matchStats == null)
                {
                    return NotFound();
                }

                return Ok(matchStats);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "An error occurred while retrieving the match stats.");
            }
        }

    }
}