using FluentValidation;

namespace S2Api.Requests;

public class IfExistEmailRequest
{
    public string Email { get; set; }
    
    public class IfExistEmailValidator : AbstractValidator<IfExistEmailRequest>
    {
        public IfExistEmailValidator()
        {
            RuleFor(x => x.Email).NotEmpty().Length(0, 320).EmailAddress();
        }
    }
}