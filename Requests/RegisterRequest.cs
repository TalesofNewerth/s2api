﻿using FluentValidation;

namespace S2Api.Requests
{
    public class RegisterRequest
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class RegisterValidator : AbstractValidator<RegisterRequest>
    {
        public RegisterValidator()
        {
            RuleFor(x => x.Username).NotEmpty().Length(0, 25).Matches("^[a-zA-Z0-9-_]+$");
            RuleFor(x => x.Email).NotEmpty().Length(0, 320).EmailAddress();
            RuleFor(x => x.Password).NotEmpty().MinimumLength(8).Matches(@"^(?=.*[A-Z])(?=.*\d)(?=.*[^A-Za-z0-9])[A-Za-z\d\s\S]{8,}$");
        }
    }
}
