﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S2Api.Requests
{
    public class SubmitReplayRequest
    {
        [BindRequired]
        public IFormFile File { get; set; }
        [BindRequired]
        public string Username { get; set; }
        [BindRequired]
        public string Password { get; set; }
    }
}
