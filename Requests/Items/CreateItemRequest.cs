using System;
using FluentValidation;
using S2Api.Enums;

namespace S2Api.Requests.Items;

public class CreateItemRequest
{
    public RuneColorEnum Color { get; set; }
    public RuneActiveEnum Active { get; set; }
    public RunePassiveEnum Passive { get; set; }
    public RuneTypeEnum Type { get; set; }
    public bool IsRandom { get; set; }
    
    public class CreateItemValidator : AbstractValidator<CreateItemRequest>
    {
        public CreateItemValidator()
        {
            RuleFor(x => x.Color).IsInEnum();
            RuleFor(x => x.Active).IsInEnum();
            RuleFor(x => x.Passive).IsInEnum();
            RuleFor(x => x.Type).IsInEnum();
        }
    }
}