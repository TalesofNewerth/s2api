﻿using PhpSerializerNET;
using S2Api.Models.MasterServer;
using S2Api.Models.Stats;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using S2Api.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using S2Api.Repository;

namespace S2Api.Services
{
    public interface ISubmitStatsService
    {
        Task<bool> SubmitStats(string login, string pass, int svr_id, int match_id, string map, int winner, string player_stats, string commander_stats, TimeOnly time, string team);
    }
    public class SubmitStatsService : ISubmitStatsService
    {
        private readonly MasterServerContext _context;
        private readonly IUserRepository _userRepo;
        public SubmitStatsService(MasterServerContext context, IUserRepository userRepo)
        {
            _context = context;
            _userRepo = userRepo;
        }

        public async Task<bool> SubmitStats(string login, string pass, int svr_id, int match_id, string map, int winner, string player_stats, string commander_stats, TimeOnly time, string team)
        {
            if (!await _userRepo.IsUserValidated(login, pass)) {
                return false;
            }

            var matchSummary = await GetMatchSummaryAsync(match_id, svr_id);

            if(matchSummary == null) {
                return false;
            }

            if (!IsLongerThanFiveMinutes(time))
            {
                return false;
            }

            if(await HasMatchAlreadyBeenSent(match_id))
            {
                return false;
            }

            var executionStrategy = _context.Database.CreateExecutionStrategy();
            return await executionStrategy.ExecuteAsync(async () =>
            {
                var transaction = await _context.Database.BeginTransactionAsync();
                try
                {
                    var commanderStats = PhpSerialization.Deserialize<List<CommanderDTO>>(commander_stats);
                    var playerStats = PhpSerialization.Deserialize<List<ActionPlayerDTO>>(player_stats);
                    var teamStats = PhpSerialization.Deserialize<List<TeamDTO>>(team);

                    if (commanderStats.Any(y => y.RecStats == 0) || playerStats.Any(y => y.RecStats == 0))
                    {
                        return false;
                    }

                    var match = new Match()
                    {
                        Id = match_id,
                        Map = map,
                        Winner = winner,
                        Duration = time
                    };

                    _context.Matches.Add(match);

                    var teamStatsToAdd = teamStats.Select(x => new Team()
                    {
                        Race = x.Race == "H" ? "1" : "2",
                        AvgSf = int.Parse(x.AvgSf),
                        Match = match_id,
                        Commander = x.Commander
                    }).ToList();

                    _context.Teams.AddRange(teamStatsToAdd);

                    await _context.SaveChangesAsync();

                    var winningTeam = teamStatsToAdd.First(x => x.Race == winner.ToString());

                    var commanderStatsToAdd = commanderStats.DistinctBy(x => x.Team).Select(x => new Commander()
                    {
                        Debuffs = x.Debuffs,
                        AccountId = x.AccountId,
                        Buffs = x.Buffs,
                        Builds = x.Builds,
                        EndStatus = x.EndStatus,
                        Exp = x.Exp,
                        Gold = x.Gold,
                        HpHealed = x.HpHealed,
                        Ip = x.Ip,
                        Kills = x.Kills,
                        Orders = x.Orders,
                        Pdmg = x.PDmg,
                        MatchId = match_id,
                        Razed = x.Razed,
                        Secs = x.Secs,
                        Sf = x.Sf,
                        TeamId = teamStatsToAdd.Where(y => y.Race == x.Team.ToString()).Select(y => y.Id).SingleOrDefault()
                    }).ToList();

                    _context.Commanders.AddRange(commanderStatsToAdd);
                    
                    var actionPlayers = playerStats.Where(x => x.Secs > 300).Select(x => new Actionplayer()
                    {
                        AccountId = x.AccountId,
                        Deaths = x.Deaths,
                        Assists = x.Assists,
                        EndStatus = x.EndStatus,
                        Exp = x.Exp,
                        Gold = x.Gold,
                        Bdmg = x.Bdmg,
                        HpHealed = x.HpHealed,
                        Ip = x.Ip,
                        Kills = x.Kills,
                        MatchId = match_id,
                        Pdmg = x.Pdmg,
                        Npc = x.Npc,
                        HpRepaired = x.HpRepaired,
                        Razed = x.Razed,
                        Res = x.Res,
                        Secs = x.Secs,
                        Sf = x.Sf,
                        Souls = x.Souls,
                        TeamId = teamStatsToAdd.Where(y => y.Race == x.Team.ToString()).Select(y => y.Id).SingleOrDefault()
                    }).ToList();

                    _context.Actionplayers.AddRange(actionPlayers);

                    await _context.SaveChangesAsync();

                    var players = actionPlayers.Select(x => (ParticipantDTO)x).Concat(commanderStatsToAdd.Select(x => (ParticipantDTO)x))
                        .Where(x => x.EndStatus == 1 && x.Secs >= 600).ToList();

                    if (players.Count >= 12)
                    {
                        await ProcessGoldEarnings(players, winningTeam.Id, matchSummary);
                    }

                    await transaction.CommitAsync();

                    return true;
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    throw;
                }
                finally
                {
                    await transaction.DisposeAsync();
                }
            }); 
        }

        private async Task<MatchSumm> GetMatchSummaryAsync(int matchId, int serverId)
        {
            return await _context.MatchSumms.FirstOrDefaultAsync(x => x.Id == matchId && serverId == x.ServerId);
        }

        private bool IsLongerThanFiveMinutes(TimeOnly time)
        {
            return time >= new TimeOnly(0, 5, 0);
        }

        private async Task<bool> HasMatchAlreadyBeenSent(int matchId)
        {
            return await _context.Matches.AnyAsync(x => x.Id == matchId);
        }

        private async Task ProcessGoldEarnings(List<ParticipantDTO> players, int winningTeamId, MatchSumm matchSummary)
        {
            //there can be duplicate players.
            var combinedPlayers = players.GroupBy(x => x.AccountId).Select(x => new ParticipantDTO()
            {
                AccountId = x.Key,
                Secs = x.Sum(y => y.Secs),
                TeamId = x.First().TeamId,
                EndStatus = x.First().EndStatus,
                Exp = x.Sum(y => y.Exp),
                Kills = x.Sum(y => y.Kills),
                Assists = x.Sum(y => y.Assists),
                HpHealed = x.Sum(y => y.HpHealed),
                Pdmg = x.Sum(y => y.Pdmg),
                Bdmg = x.Sum(y => y.Bdmg),
                HpRepaired = x.Sum(y => y.HpRepaired),
                IsCommander = x.Any(y => y.IsCommander)
            });
            
            var playerList = await _context.Users.Include(x => x.UserStash).Where(x => players.Select(y => y.AccountId).Contains(x.Id))
                .ToListAsync();
            
            bool isWeekday = IsWeekday(matchSummary.CreatedAt);
            var bestPlayers = GetBestStats(players);
            
            foreach (var player in combinedPlayers)
            {
                var account = playerList.FirstOrDefault(x => x.Id == player.AccountId);
                if (account == null)
                {
                    continue;
                }

                var gold = CalculateGold(player.Secs, player.TeamId == winningTeamId, player.IsCommander);
                gold += bestPlayers.Count(x => x.AccountId == player.AccountId) * 2;
                gold = AddGoldMultiplier(gold, isWeekday);
                if (account.UserStash == null)
                {
                    account.UserStash = new UserStash()
                    {
                        AccountId = account.Id,
                        Gold = gold,
                    };
                    _context.UserStashes.Add(account.UserStash);
                }
                else
                {
                    account.UserStash.Gold += gold;
                }
            }

            await _context.SaveChangesAsync();
        }

        private List<ParticipantDTO> GetBestStats(List<ParticipantDTO> players)
        {
            List<ParticipantDTO> bestPlayers = new List<ParticipantDTO>();
            var properties = typeof(ParticipantDTO).GetProperties();

            List<string> propertiesToIgnore = new List<string>()
            {
                "AccountId", "Secs", "TeamId", "EndStatus", "IsCommander"
            };
            
            foreach (var prop in properties)
            {
                if (propertiesToIgnore.Contains(prop.Name)) continue;

                var topPlayer = players.OrderByDescending(p => (IComparable)prop.GetValue(p)).First();
                bestPlayers.Add(topPlayer);
            }

            return bestPlayers;
        }
        
        private static bool IsWeekday(DateTime date)
        {
            DayOfWeek day = date.DayOfWeek;

            return day >= DayOfWeek.Monday && day <= DayOfWeek.Friday;
        }

        private int CalculateGold(int playTime, bool isWinner, bool isCommander)
        {
            int tenMinuteIntervals = playTime / 600;
            int gold = 2 * tenMinuteIntervals;
            if (isWinner)
            {
                gold += 2;
            }

            if (isCommander)
            {
                gold += 2;
            }

            return gold;
        }

        private int AddGoldMultiplier(int gold, bool isWeekday)
        {
            if (isWeekday)
            {
                gold *= 2;
            }

            return gold;
        }
    }
}
