using System.Threading.Tasks;
using S2Api.Repository;
using S2Api.Models;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Security.Cryptography;
using S2Api.Models.MasterServer;
using S2Api.Models.Authentication;
using Microsoft.AspNetCore.Identity;

namespace S2Api.Services
{
    public interface IAuthenticationService
    {
        Task<bool> CheckIfExists(string username, string email);
        Task<bool> VerifyEmailIfExist(string email);
        Task<User> Register(string username, string email, string password);
        Task<string> GenerateEmailConfirmationTokenAsync(User user);
        Task<EmailConfirmationToken> GetEmailConfirmationToken(string token);
        Task<User> GetUserByEmail(string email);
        Task<User> GetUserById(int id);
        Task ConfirmEmail(User user);
        Task SetLastVerificationEmailSent(User user);
        Task UpdateRefreshTokens(User user, RefreshToken token);
        Task<RefreshToken> GetRefreshToken(string token);
        Task<string> RefreshJwtToken(RefreshToken refreshToken);
        string HashPassword(string password);
        bool VerifyPassword(string password, string storedHash);
        Task<string> Authenticate(User user);
        Task<string> GeneratePasswordResetTokenAsync(User user);
        Task<IdentityResult> ResetPasswordAsync(User user, string token, string newPassword);
    }

    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUserRepository _userRepo;
        private readonly IJwtFactory _jwtFactory;

        public AuthenticationService(
            IUserRepository userRepo,
            IJwtFactory jwtFactory)
        {
            _userRepo = userRepo;
            _jwtFactory = jwtFactory;
        }

        public async Task<bool> CheckIfExists(string username, string email)
        {
            var userByUsername = await _userRepo.GetUserByUsername(username);
            var userByEmail = await _userRepo.GetUserByEmail(email);

            return userByUsername != null || userByEmail != null;
        }

        public async Task<string> Authenticate(User user)
        {
            return _jwtFactory.GenerateToken(user);
        }

        public async Task<User> Register(string username, string email, string password)
        {
            var passwordHash = HashPassword(password);
            var user = new Models.MasterServer.User
            {
                Username = username,
                Email = email,
                Password = passwordHash,
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            };

            await _userRepo.Register(user);

            return user;
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            byte[] tokenBytes = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(tokenBytes);
            }
            string token = Convert.ToBase64String(tokenBytes);

            // var emailConfirmationToken = new EmailConfirmationToken
            // {
            //     Token = token,
            //     UserId = user.Id,
            //     CreatedAt = DateTime.UtcNow,
            //     ExpirationDate = DateTime.UtcNow.AddDays(1)
            // };

            await _userRepo.CreateEmailConfirmationToken(user, token);

            return token;
        }

        public async Task<EmailConfirmationToken> GetEmailConfirmationToken(string token)
        {
            return await _userRepo.GetEmailConfirmationToken(token);
        }

        public async Task<User> GetUserById(int id)
        {
            return await _userRepo.GetUserById(id);
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _userRepo.GetUserByEmail(email);
        }

        public async Task ConfirmEmail(User user)
        {
            await _userRepo.ConfirmEmail(user);
        }

        public async Task SetLastVerificationEmailSent(User user)
        {
            await _userRepo.SetLastVerificationEmailSent(user);
        }

        public async Task UpdateRefreshTokens(User user, RefreshToken token)
        {
            await _userRepo.UpdateRefreshTokens(user, token);
        }

        public async Task<string> RefreshJwtToken(RefreshToken refreshToken)
        {
            var user = await this.GetUserById(refreshToken.UserId);
            return _jwtFactory.GenerateToken(user);
        }

        public async Task<RefreshToken> GetRefreshToken(string token)
        {
            return await _userRepo.GetRefreshToken(token);
        }

        public async Task<string> GeneratePasswordResetTokenAsync(User user)
        {
            return await _userRepo.GeneratePasswordResetTokenAsync(user);
        }

        public async Task<IdentityResult> ResetPasswordAsync(User user, string token, string newPassword)
        {
            if (user.ResetPasswordToken != token || DateTime.UtcNow > user.ResetPasswordExpiration)
            {
                return IdentityResult.Failed(new IdentityError { Description = "Invalid or expired password reset token." });
            }
            user.Password = this.HashPassword(newPassword);

            await _userRepo.ResetPasswordAsync(user);

            return IdentityResult.Success;
        }

        public string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        public bool VerifyPassword(string password, string storedHash)
        {
            return BCrypt.Net.BCrypt.Verify(password, storedHash);
        }

        public async Task<bool> VerifyEmailIfExist(string email)
        {
            return await _userRepo.DoesEmailExist(email);
        }
    }
}
