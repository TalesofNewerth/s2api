using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using S2Api.Interfaces;
using S2Api.Models;
using S2Api.Models.Items;
using OneOf;
using S2Api.Models.MasterServer;
using S2Api.Requests.Items;
using Item = S2Api.Models.Items.Item;

namespace S2Api.Services;

public interface IItemService
{
    Task<OneOf<List<Item>, IValidationError>> GetActiveItems();
    Task<OneOf<List<Item>, IValidationError>> GetStoredItems();
    Task<OneOf<Item, IValidationError>> CreateItem(CreateItemRequest request);
    Task<OneOf<int, IValidationError>> GetGold();
    Task<OneOf<int, IValidationError>> SalvageItem(int itemId);
    Task<OneOf<int, IValidationError>> ActivateItem(int itemId);
    Task<OneOf<bool, IValidationError>> DeleteActiveItem(int itemId);
    List<ItemConfiguration> GetAffixes();
}

public class ItemService : IItemService
{
    private readonly MasterServerContext _context;
    private readonly IHttpContextAccessor _httpContextAccessor;
    private readonly List<ItemConfiguration> _itemConfigurations;
    private readonly int _currentAccountId;

    public ItemService(MasterServerContext context, IHttpContextAccessor httpContextAccessor, IOptionsSnapshot<List<ItemConfiguration>> options)
    {
        _httpContextAccessor = httpContextAccessor;
        _context = context;
        _currentAccountId = GetAccountId();
        _itemConfigurations = options.Value;
    }

    /// <summary>
    /// Retrieves a list of active items for the current account.
    /// </summary>
    /// <returns>A task that represents the asynchronous operation. The task result contains either a list of active items or a validation error.</returns>
    public async Task<OneOf<List<Item>, IValidationError>> GetActiveItems()
    {
        if (_currentAccountId == 0)
        {
            return new ValidationError("Invalid account id");
        }

        var currentDate = DateOnly.FromDateTime(DateTime.UtcNow);

        var items = await _context.Items
            .Where(item => item.AccountId == _currentAccountId && (item.ExpDate == null || item.ExpDate > currentDate))
            .ToListAsync();

        return items.Select(x => new Item(_itemConfigurations, x)).ToList();
    }

    /// <summary>
    /// Retrieves a list of stored items for the current user's account.
    /// </summary>
    /// <returns>A task that represents the asynchronous operation. The task result contains either a list of stored items or a validation error.</returns>
    public async Task<OneOf<List<Item>, IValidationError>> GetStoredItems()
    {
        if (_currentAccountId == 0)
        {
            return new ValidationError("Invalid account id");
        }

        var userStash = await _context.UserStashes.Include(x => x.StoredItems)
            .Where(s => s.AccountId == _currentAccountId)
            .FirstOrDefaultAsync() ?? await CreateUserStashIfNotExist();

        var storedItems = userStash.StoredItems
            .Where(s => s.StashId == userStash.Id && s.Type != null && s.CreationDate != null)
            .Select(item => new StoredItem()
            {
                Id = item.Id,
                Type = item.Type.Value,
                CreationDate = item.CreationDate.Value,
            }).ToList();

        var items = storedItems.Select(x => new Item(_itemConfigurations, x)).ToList();

        return items;
    }

    /// <summary>
    /// Creates a new item based on the provided request and saves it to the database.
    /// </summary>
    /// <param name="request">The request containing the details of the item to be created.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains either the ID of the newly created item or a validation error.</returns>
    public async Task<OneOf<Item, IValidationError>> CreateItem(CreateItemRequest request)
    {
        if (_currentAccountId == 0)
        {
            return new ValidationError("Invalid account id");
        }

        var userStash = await _context.UserStashes
            .Where(s => s.AccountId == _currentAccountId)
            .FirstOrDefaultAsync() ?? await CreateUserStashIfNotExist();

        Item item = request.IsRandom
            ? new Item(_itemConfigurations)
            : new Item(_itemConfigurations, request.Type, request.Color, request.Passive, request.Active);

        if (!item.ValidateRune())
        {
            return new ValidationError("Invalid item");
        }

        var totalCost = item.GetGoldCost();

        if (totalCost > userStash.Gold)
        {
            return new ValidationError("Not enough gold");
        }

        var newItem = new Models.MasterServer.StoredItem
        {
            StashId = userStash.Id,
            Type = item.GetNumber(),
            CreationDate = DateTime.UtcNow
        };

        userStash.Gold -= totalCost;

        _context.StoredItems.Add(newItem);
        await _context.SaveChangesAsync();
        item.Id = newItem.Id;
        //return item for random.
        return item;
    }

    /// <summary>
    /// Retrieves the amount of gold for the current account.
    /// </summary>
    /// <returns>A task that represents the asynchronous operation. The task result contains either the amount of gold or a validation error.</returns>
    public async Task<OneOf<int, IValidationError>> GetGold()
    {
        if (_currentAccountId == 0)
        {
            return new ValidationError("Invalid account id");
        }

        var userStash = await _context.UserStashes
            .Include(s => s.StoredItems)
            .FirstOrDefaultAsync(s => s.AccountId == _currentAccountId) ?? await CreateUserStashIfNotExist();

        return userStash.Gold;
    }

    /// <summary>
    /// Retrieves a list of item configurations, representing different affixes.
    /// </summary>
    /// <returns>A list of item configurations containing information about the affixes.</returns>
    public List<ItemConfiguration> GetAffixes()
    {
        return _itemConfigurations;
    }

    /// Salvages an item and updates the user stash gold.
    /// @param itemId The ID of the item to be salvaged.
    /// @returns A task that represents the asynchronous operation. The task result contains either the updated user stash gold or a validation error.
    /// /
    public async Task<OneOf<int, IValidationError>> SalvageItem(int itemId)
    {
        if (_currentAccountId == 0)
        {
            return new ValidationError("Invalid account id");
        }

        var userStash = await _context.UserStashes
            .Include(s => s.StoredItems)
            .FirstOrDefaultAsync(s => s.AccountId == _currentAccountId) ?? await CreateUserStashIfNotExist();

        var itemToSalvage = userStash.StoredItems.FirstOrDefault(item => item.Id == itemId);

        if (itemToSalvage?.Type == null)
            return new ValidationError("Invalid request");

        var item = new Item(_itemConfigurations, itemToSalvage.Type.Value);

        // Update user stash gold
        userStash.Gold += Item.GetSalvagedValue();

        // Remove the item from user stash
        userStash.StoredItems.Remove(itemToSalvage);

        // Save changes to the database
        await _context.SaveChangesAsync();

        return userStash.Gold;
    }

    /// <summary>
    /// Activates an item with the specified item ID for the current account.
    /// </summary>
    /// <param name="itemId">The ID of the item to be activated.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains either the ID of the activated item or a validation error.</returns>
    public async Task<OneOf<int, IValidationError>> ActivateItem(int itemId)
    {
        if (_currentAccountId == 0)
        {
            return new ValidationError("Invalid account id");
        }

        var userStash = await _context.UserStashes
            .Include(s => s.StoredItems)
            .FirstOrDefaultAsync(s => s.AccountId == _currentAccountId) ?? await CreateUserStashIfNotExist();

        var itemToMove = userStash.StoredItems.FirstOrDefault(item => item.Id == itemId);

        if (itemToMove == null)
            return new ValidationError("Invalid request");

        // Additional validation: Check if user already has the maximum number of active items
        var activeItemsCount = await _context.Items
            .Where(i => i.AccountId == userStash.AccountId && i.ExpDate > DateOnly.FromDateTime(DateTime.UtcNow))
            .CountAsync();

        if (activeItemsCount >= 5)
            return new ValidationError("You already have the maximum number of active items.");

        // Add the item to the Items table
        var item = new Models.MasterServer.Item()
        {
            AccountId = userStash.AccountId,
            ExpDate = DateOnly.FromDateTime(DateTime.UtcNow.AddMonths(1)),
            Type = itemToMove.Type
        };

        _context.Items.Add(item);

        // Remove the item from storedItems
        userStash.StoredItems.Remove(itemToMove);

        // Save changes to the database
        await _context.SaveChangesAsync();

        return item.Id;
    }

    /// <summary>
    /// Deletes the active item with the specified item ID for the current account.
    /// </summary>
    /// <param name="itemId">The ID of the item to be deleted.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains a boolean indicating whether the deletion was successful or a validation error.</returns>
    public async Task<OneOf<bool, IValidationError>> DeleteActiveItem(int itemId)
    {
        if (_currentAccountId == 0)
        {
            return new ValidationError("Invalid account id");
        }

        var itemToDelete = await _context.Items.FirstOrDefaultAsync(item => item.Id == itemId && item.AccountId == _currentAccountId);
        
        if (itemToDelete.ExpDate <= DateOnly.FromDateTime(DateTime.UtcNow))
            return new ValidationError("Cannot delete an expired item.");
        
        // Delete the item from the Items table
        _context.Items.Remove(itemToDelete);

        // Save changes to the database
        await _context.SaveChangesAsync();

        return true;
    }

    /// <summary>
    /// Retrieves the account ID for the current user.
    /// </summary>
    /// <returns>
    /// An integer representing the account ID of the current user.
    /// Returns 0 if the account ID cannot be retrieved or parsed.
    /// </returns>
    private int GetAccountId()
    {
        var accountIdString = _httpContextAccessor.HttpContext?.User.Claims.Where(c => c.Type == "accountId")
            .Select(c => c.Value)
            .FirstOrDefault();

        return int.TryParse(accountIdString, out var id) ? id : 0;
    }

    /// <summary>
    /// Creates a user stash if it does not exist for the current account. This method is called when trying to access or modify the user stash and there is no existing stash for the current account.
    /// </summary>
    /// <returns>A task that represents the asynchronous operation. The task result contains the created user stash.</returns>
    private async Task<UserStash> CreateUserStashIfNotExist()
    {
        var userStash = new UserStash()
        {
            AccountId = _currentAccountId,
            Gold = 0,
            StoredItems = new List<StoredItem>()
        };
        _context.UserStashes.Add(userStash);
        await _context.SaveChangesAsync();
        return userStash;
    }
}