﻿using Discord.Webhook;
using Microsoft.Extensions.Configuration;
using S2Api.Models.Discord;
using S2Api.Utils;
using System;
using System.Threading.Tasks;

namespace S2Api.Services
{
    public interface IDiscordWebhookService
    {
        Task<ulong> SendMessage(DiscordMessageDTO message);
        Task EditMessage(ulong id, DiscordMessageDTO message);
    }
    public class DiscordWebhookService : IDiscordWebhookService
    {
        private readonly DiscordWebhookClient _client;
        public DiscordWebhookService(IConfiguration config)
        {
            _client = new DiscordWebhookClient(Environment.GetEnvironmentVariable("WEB_HOOK_URL").IfEmptyThenUse(config["WebHookUrl"]));
        }

        public async Task<ulong> SendMessage(DiscordMessageDTO message)
        {
            return await _client.SendMessageAsync(message.Message, false, message.Embeds);
        }

        public async Task EditMessage(ulong id, DiscordMessageDTO message)
        {
            await _client.ModifyMessageAsync(id, m =>
            {
                m.Embeds = message.Embeds;
                m.Content = string.IsNullOrEmpty(message.Message) ? m.Content : message.Message;
            });
        }
    }
}
