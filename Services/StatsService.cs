using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using S2Api.Models;
using S2Api.Models.MasterServer;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace S2Api.Services
{
    public interface IStatsService
    {
        Task<GetAllMatchesResponse> GetAllMatchesAsync(int page, int pageSize, string sortBy, bool isAscending, string filter);
        Task<MatchResponse> GetMatchByIdAsync(int matchId);
        Task<MatchResponse> GetMatchStatsByIdAsync(int matchId);
        Task<GetAllPlayerStatsResponse> GetAllPlayerStatsAsync(int page, int pageSize, string sortBy, bool isAscending, string filter);
        Task<PlayerStatsResponse> GetPlayerStatsByUsernameAsync(string username);
    }


    public class StatsService : IStatsService
    {
        private readonly IMapper _mapper;
        private readonly MasterServerContext _dbContext;

        public StatsService(IMapper mapper, MasterServerContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        /* Recent Matches */
        public async Task<GetAllMatchesResponse> GetAllMatchesAsync(int page = 1, int pageSize = 25, string sortBy = "createdAt", bool isAscending = false, string filter = null)
        {
            // Fetch both Matches and MatchSumm records and combine them
            var matchesWithSumms = await (from match in _dbContext.Matches
                                          join summ in _dbContext.MatchSumms on match.Id equals summ.Id
                                          select new
                                          {
                                              Match = match,
                                              MatchSumm = summ,
                                          }).ToListAsync();

            // Apply filtering based on the filter parameter
            if (!string.IsNullOrEmpty(filter))
            {
                var userAccountId = await _dbContext.Users
                    .Where(user => user.Username == filter)
                    .Select(user => user.Id)
                    .FirstOrDefaultAsync();

                var actionPlayersForUser = await _dbContext.Actionplayers
                    .Where(ap => ap.AccountId == userAccountId)
                    .ToListAsync();

                var commandersForUser = await _dbContext.Commanders
                    .Where(c => c.AccountId == userAccountId)
                    .ToListAsync();

                matchesWithSumms = matchesWithSumms.Where(ms =>
                    actionPlayersForUser.Any(ap => ap.MatchId == ms.Match.Id) ||
                    commandersForUser.Any(c => c.MatchId == ms.Match.Id)
                ).ToList();
            }

            // Apply sorting based on sortBy and isAscending parameters
            switch (sortBy.ToLower())
            {
                case "id":
                    matchesWithSumms = isAscending ? matchesWithSumms.OrderBy(ms => ms.Match.Id).ToList() : matchesWithSumms.OrderByDescending(ms => ms.Match.Id).ToList();
                    break;
                case "winner":
                    matchesWithSumms = isAscending ? matchesWithSumms.OrderBy(ms => ms.Match.Winner).ToList() : matchesWithSumms.OrderByDescending(ms => ms.Match.Winner).ToList();
                    break;
                case "duration":
                    matchesWithSumms = isAscending ? matchesWithSumms.OrderBy(ms => ms.Match.Duration).ToList() : matchesWithSumms.OrderByDescending(ms => ms.Match.Duration).ToList();
                    break;
                case "map":
                    matchesWithSumms = isAscending ? matchesWithSumms.OrderBy(ms => ms.Match.Map).ToList() : matchesWithSumms.OrderByDescending(ms => ms.Match.Map).ToList();
                    break;
                case "createdAt":
                    matchesWithSumms = isAscending ? matchesWithSumms.OrderBy(ms => ms.MatchSumm.CreatedAt).ToList() : matchesWithSumms.OrderByDescending(ms => ms.MatchSumm.CreatedAt).ToList();
                    break;
                default:
                    matchesWithSumms = isAscending ? matchesWithSumms.OrderBy(ms => ms.Match.Id).ToList() : matchesWithSumms.OrderByDescending(ms => ms.Match.Id).ToList();
                    break;
            }

            var totalMatches = matchesWithSumms.Count();

            // Apply paging based on page and pageSize parameters
            var pagedMatchesWithSumms = matchesWithSumms.Skip((page - 1) * pageSize).Take(pageSize);

            // Convert the combined data to MatchResponse
            var matchResponses = pagedMatchesWithSumms.Select(ms => new MatchResponse
            {
                Id = ms.Match.Id,
                Winner = ms.Match.Winner,
                Duration = new TimeSpan(ms.Match.Duration.Hour, ms.Match.Duration.Minute, ms.Match.Duration.Second),
                Map = ms.Match.Map,
                CreatedAt = ms.MatchSumm.CreatedAt,
                // You can add more fields here if needed
            }).ToList();

            // Create the GetAllMatchesResponse object
            var result = new GetAllMatchesResponse
            {
                Matches = matchResponses,
                TotalCount = totalMatches
            };

            return result;
        }

        public async Task<MatchResponse> GetMatchByIdAsync(int matchId)
        {
            // Fetch both Match and MatchSumm records for the given matchId
            var matchWithSumm = await (from match in _dbContext.Matches
                                       join summ in _dbContext.MatchSumms on match.Id equals summ.Id
                                       where match.Id == matchId
                                       select new
                                       {
                                           Match = match,
                                           MatchSumm = summ
                                       }).FirstOrDefaultAsync();

            if (matchWithSumm == null)
            {
                return null;
            }

            // Convert the combined data to MatchResponse
            var matchResponse = new MatchResponse
            {
                Id = matchWithSumm.Match.Id,
                Winner = matchWithSumm.Match.Winner,
                Duration = new TimeSpan(matchWithSumm.Match.Duration.Hour, matchWithSumm.Match.Duration.Minute, matchWithSumm.Match.Duration.Second),
                Map = matchWithSumm.Match.Map,
                CreatedAt = matchWithSumm.MatchSumm.CreatedAt,
                // You can add more fields here if needed
            };

            return matchResponse;
        }

        public async Task<MatchResponse> GetMatchStatsByIdAsync(int matchId)
        {
            // Fetch both Match and MatchSumm records for the given matchId
            var matchWithSumm = await (from match in _dbContext.Matches
                                       join summ in _dbContext.MatchSumms on match.Id equals summ.Id
                                       where match.Id == matchId
                                       select new
                                       {
                                           Match = match,
                                           MatchSumm = summ
                                       }).FirstOrDefaultAsync();

            if (matchWithSumm == null)
            {
                return null;
            }

            // Fetch teams data for the given matchId
            var teams = await (from team in _dbContext.Teams
                               where team.Match == matchId
                               select team).ToListAsync();

            // Fetch actionplayers data for the given matchId
            var actionPlayers = await (from actionPlayer in _dbContext.Actionplayers
                                       where actionPlayer.MatchId == matchId
                                       select actionPlayer).ToListAsync();

            var actionPlayerResponses = _mapper.Map<List<ActionPlayerResponse>>(actionPlayers);

            foreach (var actionPlayerResponse in actionPlayerResponses)
            {
                var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Id == actionPlayerResponse.AccountId);
                if (user != null)
                {
                    actionPlayerResponse.Username = user.Username;
                }
            }

            var commanders = await (from commander in _dbContext.Commanders
                                    where commander.MatchId == matchId
                                    select commander).ToListAsync();

            var commanderResponses = _mapper.Map<List<CommanderResponse>>(commanders);

            foreach (var commanderResponse in commanderResponses)
            {
                var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Id == commanderResponse.AccountId);
                if (user != null)
                {
                    commanderResponse.Username = user.Username;
                }
            }

            // Convert the combined data to MatchResponse
            var matchResponse = new MatchStatsResponse
            {
                Id = matchWithSumm.Match.Id,
                Winner = matchWithSumm.Match.Winner,
                Duration = new TimeSpan(matchWithSumm.Match.Duration.Hour, matchWithSumm.Match.Duration.Minute, matchWithSumm.Match.Duration.Second),
                Map = matchWithSumm.Match.Map,
                CreatedAt = matchWithSumm.MatchSumm.CreatedAt,
                Commanders = commanderResponses,
                Teams = _mapper.Map<List<TeamResponse>>(teams),
                ActionPlayers = actionPlayerResponses,
            };

            return matchResponse;
        }



        /* Global leaderboard */
        public async Task<GetAllPlayerStatsResponse> GetAllPlayerStatsAsync(int page = 1, int pageSize = 25, string sortBy = "sf", bool isAscending = false, string filter = null)
        {
            if (string.IsNullOrWhiteSpace(sortBy) || sortBy.ToLower() == "null")
            {
                // Set sortBy to the default value ("sf" in this case)
                sortBy = "sf";
            }

            var playerStats = await (from player in _dbContext.Playerstats
                                    join user in _dbContext.Users
                                    on player.AccountId equals user.Id
                                    join playerInfo in _dbContext.Playerinfos
                                    on player.AccountId equals playerInfo.AccountId
                                    select new
                                    {
                                        Player = player,
                                        user.Username,
                                        playerInfo.Sf
                                    }).ToListAsync();

            // Apply filtering based on the filter parameter
            if (!string.IsNullOrEmpty(filter))
            {
                playerStats = playerStats.Where(ps => ps.Username == filter).ToList();
            }

            // Apply sorting based on sortBy and isAscending parameters
            switch (sortBy.ToLower())
            {
                case "username":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Username).ToList() : playerStats.OrderByDescending(p => p.Username).ToList();
                    break;
                case "exp":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Exp).ToList() : playerStats.OrderByDescending(p => p.Player.Exp).ToList();
                    break;
                case "secs":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Secs).ToList() : playerStats.OrderByDescending(p => p.Player.Secs).ToList();
                    break;
                case "gold":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Gold).ToList() : playerStats.OrderByDescending(p => p.Player.Gold).ToList();
                    break;
                case "hp_repaired":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.HpRepaired).ToList() : playerStats.OrderByDescending(p => p.Player.HpRepaired).ToList();
                    break;
                case "res":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Res).ToList() : playerStats.OrderByDescending(p => p.Player.Res).ToList();
                    break;
                case "hp_healed":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.HpHealed).ToList() : playerStats.OrderByDescending(p => p.Player.HpHealed).ToList();
                    break;
                case "npc":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Npc).ToList() : playerStats.OrderByDescending(p => p.Player.Npc).ToList();
                    break;
                case "bdmg":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Bdmg).ToList() : playerStats.OrderByDescending(p => p.Player.Bdmg).ToList();
                    break;
                case "pdmg":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Pdmg).ToList() : playerStats.OrderByDescending(p => p.Player.Pdmg).ToList();
                    break;
                case "razed":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Razed).ToList() : playerStats.OrderByDescending(p => p.Player.Razed).ToList();
                    break;
                case "souls":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Souls).ToList() : playerStats.OrderByDescending(p => p.Player.Souls).ToList();
                    break;
                case "assists":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Assists).ToList() : playerStats.OrderByDescending(p => p.Player.Assists).ToList();
                    break;
                case "deaths":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Deaths).ToList() : playerStats.OrderByDescending(p => p.Player.Deaths).ToList();
                    break;
                case "kills":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Kills).ToList() : playerStats.OrderByDescending(p => p.Player.Kills).ToList();
                    break;
                case "ties":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Ties).ToList() : playerStats.OrderByDescending(p => p.Player.Ties).ToList();
                    break;
                case "losses":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Losses).ToList() : playerStats.OrderByDescending(p => p.Player.Losses).ToList();
                    break;
                case "wins":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.Wins).ToList() : playerStats.OrderByDescending(p => p.Player.Wins).ToList();
                    break;
                case "d_conns":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Player.DConns).ToList() : playerStats.OrderByDescending(p => p.Player.DConns).ToList();
                    break;
                case "sf":
                    playerStats = isAscending ? playerStats.OrderBy(p => p.Sf).ToList() : playerStats.OrderByDescending(p => p.Sf).ToList();
                    break;
                case "kd":
                    playerStats = isAscending ? playerStats.OrderBy(p => Helpers.GetKD(p.Player.Kills, p.Player.Deaths)).ToList() : playerStats.OrderByDescending(p => Helpers.GetKD(p.Player.Kills, p.Player.Deaths)).ToList();
                    break;
                case "winrate":
                    playerStats = isAscending ? playerStats.OrderBy(p => Helpers.GetWinrate(p.Player.Wins, p.Player.DConns, p.Player.Ties, p.Player.Losses)).ToList() : playerStats.OrderByDescending(p => Helpers.GetWinrate(p.Player.Wins, p.Player.DConns, p.Player.Ties, p.Player.Losses)).ToList();
                    break;
                default:
                    break;
            }

            // Filter out players with less than 10 games played or less than 5 hours of gametime
            playerStats = playerStats.Where(ps => (ps.Player.Wins + ps.Player.Losses) > 10 && ps.Player.Secs >= 18000).ToList();

            var totalMatches = playerStats.Count();

            // Apply paging based on page and pageSize parameters
            var pagedMatchesWithSumms = playerStats.Skip((page - 1) * pageSize).Take(pageSize);

            // Convert the combined data to MatchResponse
            var playerResponses = pagedMatchesWithSumms.Select(ps => new PlayerStatsResponse
            {
                Username = ps.Username,
                AccountId = ps.Player.AccountId,
                Exp = ps.Player.Exp,
                Wins = ps.Player.Wins,
                DConns = ps.Player.DConns,
                Losses = ps.Player.Losses,
                Ties = ps.Player.Ties,
                Kills = ps.Player.Kills,
                Deaths = ps.Player.Deaths,
                Assists = ps.Player.Assists,
                Souls = ps.Player.Souls,
                Razed = ps.Player.Razed,
                Pdmg = ps.Player.Pdmg,
                Bdmg = ps.Player.Bdmg,
                Npc = ps.Player.Npc,
                HpHealed = ps.Player.HpHealed,
                Res = ps.Player.Res,
                HpRepaired = ps.Player.HpRepaired,
                Gold = ps.Player.Gold,
                Secs = ps.Player.Secs
            }).ToList();

            var result = new GetAllPlayerStatsResponse
            {
                Players = playerResponses,
                TotalCount = totalMatches
            };

            return result;
        }

        public async Task<PlayerStatsResponse> GetPlayerStatsByUsernameAsync(string username)
        {
            // Fetch the player stats for the given username
            var playerStat = await (from player in _dbContext.Playerstats
                            join user in _dbContext.Users on player.AccountId equals user.Id
                            join playerInfo in _dbContext.Playerinfos on player.AccountId equals playerInfo.AccountId
                            where user.Username == username
                            select new
                            {
                                Player = player,
                                user.Username,
                                playerInfo.Sf
                            }).FirstOrDefaultAsync();

            if (playerStat == null)
            {
                return null;
            }

            // Fetch all player stats from the database
            var allPlayerStats = await (from player in _dbContext.Playerstats
                                        select player).ToListAsync();

            var includedProperties = new List<string>
            {
                nameof(PlayerStatsResponse.Exp),
                nameof(PlayerStatsResponse.Wins),
                nameof(PlayerStatsResponse.DConns),
                nameof(PlayerStatsResponse.Losses),
                nameof(PlayerStatsResponse.Ties),
                nameof(PlayerStatsResponse.Kills),
                nameof(PlayerStatsResponse.Deaths),
                nameof(PlayerStatsResponse.Assists),
                nameof(PlayerStatsResponse.Souls),
                nameof(PlayerStatsResponse.Razed),
                nameof(PlayerStatsResponse.Pdmg),
                nameof(PlayerStatsResponse.Bdmg),
                nameof(PlayerStatsResponse.Npc),
                nameof(PlayerStatsResponse.HpHealed),
                nameof(PlayerStatsResponse.Res),
                nameof(PlayerStatsResponse.HpRepaired),
                nameof(PlayerStatsResponse.Gold),
                nameof(PlayerStatsResponse.Secs)
            };

            var rankStats = new Dictionary<string, int>();

            // Calculate rank for each stat
            var statProperties = typeof(PlayerStatsResponse).GetProperties();
            foreach (var statProperty in statProperties)
            {
                if (!includedProperties.Contains(statProperty.Name))
                {
                    continue;
                }

                var statName = statProperty.Name;
                var propertyInfo = playerStat.Player.GetType().GetProperty(statName);
                var playerStatValue = (int)propertyInfo.GetValue(playerStat.Player);

                // Sort all player stats by the current stat in descending order
                var sortedStats = allPlayerStats.OrderByDescending(p =>
                {
                    var pInfo = p.GetType().GetProperty(statName);
                    return (int)pInfo.GetValue(p);
                }).ToList();

                // Find the player's rank for the current stat
                var rank = sortedStats.FindIndex(p =>
                {
                    var pInfo = p.GetType().GetProperty(statName);
                    return (int)pInfo.GetValue(p) == playerStatValue;
                }) + 1;
                rankStats.Add(statName, rank);
            }

            var receivedKarma = await (from karma in _dbContext.Karmas
                                       join user in _dbContext.Users on karma.AccountId equals user.Id
                                       where karma.TargetId == playerStat.Player.AccountId
                                       select new KarmaResponse
                                       {
                                           Username = user.Username,
                                           TargetUsername = playerStat.Username,
                                           MatchId = karma.MatchId,
                                           Date = karma.Match.CreatedAt,
                                           Do = karma.Do,
                                           Reason = karma.Reason
                                       }).ToListAsync();

            var sentKarma = await (from karma in _dbContext.Karmas
                                   join user in _dbContext.Users on karma.TargetId equals user.Id
                                   where karma.AccountId == playerStat.Player.AccountId
                                   select new KarmaResponse
                                   {
                                       Username = playerStat.Username,
                                       TargetUsername = user.Username,
                                       MatchId = karma.MatchId,
                                       Date = karma.Match.CreatedAt,
                                       Do = karma.Do,
                                       Reason = karma.Reason
                                   }).ToListAsync();

            var playerStatsResponse = new PlayerStatsResponse
            {
                Username = playerStat.Username,
                AccountId = playerStat.Player.AccountId,
                Exp = playerStat.Player.Exp,
                Wins = playerStat.Player.Wins,
                DConns = playerStat.Player.DConns,
                Losses = playerStat.Player.Losses,
                Ties = playerStat.Player.Ties,
                Kills = playerStat.Player.Kills,
                Deaths = playerStat.Player.Deaths,
                Assists = playerStat.Player.Assists,
                Souls = playerStat.Player.Souls,
                Razed = playerStat.Player.Razed,
                Pdmg = playerStat.Player.Pdmg,
                Bdmg = playerStat.Player.Bdmg,
                Npc = playerStat.Player.Npc,
                HpHealed = playerStat.Player.HpHealed,
                Res = playerStat.Player.Res,
                HpRepaired = playerStat.Player.HpRepaired,
                Gold = playerStat.Player.Gold,
                Secs = playerStat.Player.Secs,
                RankStats = rankStats,
                ReceivedKarma = receivedKarma,
                SentKarma = sentKarma,
                SF = playerStat.Sf
            };

            return playerStatsResponse;
        }
    }

    internal class MatchStatsResponse : MatchResponse
    {
        public List<CommanderResponse> Commanders { get; set; }
        public List<TeamResponse> Teams { get; set; }
        public List<ActionPlayerResponse> ActionPlayers { get; set; }
    }

    public class CommanderResponse
    {
        public string Username { get; set; }
        public int AccountId { get; set; }
        public int MatchId { get; set; }
        public int TeamId { get; set; }
        public int Builds { get; set; }
        public int Exp { get; set; }
        public int Gold { get; set; }
        public int Razed { get; set; }
        public int HpHealed { get; set; }
        public int Pdmg { get; set; }
        public int Kills { get; set; }
        public int Debuffs { get; set; }
        public int Buffs { get; set; }
        public int Orders { get; set; }
        public int Secs { get; set; }
        public int? EndStatus { get; set; }
        public int? Sf { get; set; }
    }

    public class TeamResponse
    {
        public int Id { get; set; }
        public int Match { get; set; }
        public string Race { get; set; }
        public int? AvgSf { get; set; }
        public string Commander { get; set; }
    }

    public class ActionPlayerResponse
    {
        public string Username { get; set; }
        public int AccountId { get; set; }
        public int MatchId { get; set; }
        public int TeamId { get; set; }
        public int Exp { get; set; }
        public int Kills { get; set; }
        public int Deaths { get; set; }
        public int Assists { get; set; }
        public int Souls { get; set; }
        public int Razed { get; set; }
        public int Pdmg { get; set; }
        public int Bdmg { get; set; }
        public int Npc { get; set; }
        public int HpHealed { get; set; }
        public int Res { get; set; }
        public int Gold { get; set; }
        public int HpRepaired { get; set; }
        public int Secs { get; set; }
        public int EndStatus { get; set; }
        public int Sf { get; set; }
    }

    public class MatchResponse
    {
        public int Id { get; set; }
        public int? Winner { get; set; }
        public TimeSpan Duration { get; set; }
        public string Map { get; set; }
        public DateTime CreatedAt { get; set; }
    }

    public class GetAllMatchesResponse
    {
        public List<MatchResponse> Matches { get; set; }
        public int TotalCount { get; set; }
    }

    public class PlayerStatsResponse
    {
        public string Username { get; set; }
        // public int Rank { get; set; }
        public int AccountId { get; set; }
        public int? Exp { get; set; }
        public int? Wins { get; set; }
        public int? DConns { get; set; }
        public int? Losses { get; set; }
        public int? Ties { get; set; }
        public int? Kills { get; set; }
        public int? Deaths { get; set; }
        public int? Assists { get; set; }
        public int? Souls { get; set; }
        public int? Razed { get; set; }
        public int? Pdmg { get; set; }
        public int? Bdmg { get; set; }
        public int? Npc { get; set; }
        public int? HpHealed { get; set; }
        public int? Res { get; set; }
        public int? HpRepaired { get; set; }
        public int? Gold { get; set; }
        public int? Secs { get; set; }
        public Dictionary<string, int> RankStats { get; set; }
        public List<KarmaResponse> ReceivedKarma { get; set; }
        public List<KarmaResponse> SentKarma { get; set; }

        public int? SF { get; set; }
        // public int SF => Helpers.GetSF(Exp, Secs);
        public double KD => Helpers.GetKD(Kills, Deaths);
        public double WinRate => Helpers.GetWinrate(Wins, DConns, Ties, Losses);
    }

    public class KarmaResponse
    {
        public string Username { get; set; }
        public string TargetUsername { get; set; }
        public int MatchId { get; set; }
        public DateTime Date { get; set; }
        public string Do { get; set; }
        public string Reason { get; set; }
    }

    public class GetAllPlayerStatsResponse
    {
        public List<PlayerStatsResponse> Players { get; set; }
        public int TotalCount { get; set; }
    }

    public static class Helpers
    {
        public static int GetSF(int? exp, int? secs)
        {
            return exp.HasValue && secs.HasValue && secs.Value != 0 ? exp.Value / (secs.Value / 60) : 0;
        }

        public static double GetKD(int? kills, int? deaths)
        {
            return deaths.HasValue && deaths.Value != 0 ? Math.Round((double)kills.GetValueOrDefault() / deaths.Value, 2) : 0;
        }

        public static double GetWinrate(int? wins, int? dConns, int? ties, int? losses)
        {
            double totalGames = wins.GetValueOrDefault() + dConns.GetValueOrDefault() + ties.GetValueOrDefault() + losses.GetValueOrDefault();
            return totalGames != 0
                    ? Math.Round((double)wins.GetValueOrDefault() / totalGames * 100, 2)
                    : 0;
        }
    }
}