using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace S2Api.Services
{
    public interface IEventService
    {
        void AddEventStreamWriter(int accountId, EventStreamWriter streamWriter);
        bool TryGetEventStreamWriter(int accountId, out EventStreamWriter streamWriter);
        void RemoveEventStreamWriter(int accountId);
        Task SendEventAsync(int accountId, string eventName, object eventData);
    }

    public class EventService : IEventService
    {
        private readonly Dictionary<int, EventStreamWriter> _streamWriters;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public EventService(IHttpContextAccessor httpContextAccessor)
        {
            _streamWriters = new Dictionary<int, EventStreamWriter>();
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task SendEventAsync(int accountId, string eventName, object eventData)
        {
            if (_streamWriters.TryGetValue(accountId, out var eventStreamWriter))
            {
                var message = $"event: {eventName}\ndata: {JsonConvert.SerializeObject(eventData)}\n\n";
                await eventStreamWriter.WriteAsync(message);
                await eventStreamWriter.FlushAsync();
            }
        }

        public void AddEventStreamWriter(int accountId, EventStreamWriter streamWriter)
        {
            _streamWriters[accountId] = streamWriter;
        }

        public bool TryGetEventStreamWriter(int accountId, out EventStreamWriter streamWriter)
        {
            return _streamWriters.TryGetValue(accountId, out streamWriter);
        }

        public void RemoveEventStreamWriter(int accountId)
        {
            _streamWriters.Remove(accountId);
        }
    }

    public class EventStreamWriter : IDisposable
    {
        private readonly StreamWriter _streamWriter;

        public EventStreamWriter(Stream stream)
        {
            _streamWriter = new StreamWriter(stream, Encoding.UTF8);
        }

        public async Task WriteAsync(string data)
        {
            await _streamWriter.WriteAsync(data);
        }

        public async Task FlushAsync()
        {
            await _streamWriter.FlushAsync();
        }

        public void Dispose()
        {
            _streamWriter.DisposeAsync();
        }
    }
}