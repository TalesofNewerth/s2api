﻿namespace S2Api.Enums;

public enum RunePassiveEnum
{
    /// <summary>
    /// mana
    /// </summary>
    Dolphin = 1,
    /// <summary>
    /// gold
    /// </summary>
    Beaver = 2,
    /// <summary>
    /// evasion
    /// </summary>
    Fox = 3,
    /// <summary>
    /// armor
    /// </summary>
    Armadillo = 4,
    /// <summary>
    /// health
    /// </summary>
    Bear = 5,
    /// <summary>
    /// movement speed
    /// </summary>
    Lynx = 6,
    /// <summary>
    /// stamina
    /// </summary>
    Rabbit = 7 
}