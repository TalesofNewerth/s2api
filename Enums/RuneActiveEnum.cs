﻿namespace S2Api.Enums;

public enum RuneActiveEnum
{
    Lung = 1, 
    Heart = 2,
    Shield = 3,
    Feet = 4,
    Brain = 5,
    Dagger = 6
}