﻿namespace S2Api.Enums;

public enum RuneColorEnum
{
    /// <summary>
    /// Hp regen
    /// </summary>
    Red = 1,
    /// <summary>
    /// Gold
    /// </summary>
    Yellow = 2,
    /// <summary>
    /// Mana
    /// </summary>
    Blue = 3,
    /// <summary>
    /// Stamina
    /// </summary>
    White = 4
}