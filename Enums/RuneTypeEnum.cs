﻿namespace S2Api.Enums;

public enum RuneTypeEnum
{
    Ring = 1,
    Amulet = 2,
    Jewel = 3,
    Rune = 4
}