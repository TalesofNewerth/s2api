using AutoMapper;
using S2Api.Models.MasterServer;
using S2Api.Services;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<Commander, CommanderResponse>();
        CreateMap<Team, TeamResponse>();
        CreateMap<Actionplayer, ActionPlayerResponse>();
        CreateMap<Karma, KarmaResponse>();
    }
}