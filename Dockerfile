#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

#ENV ASPNETCORE_ENVIRONMENT Development
ENV ASPNETCORE_ENVIRONMENT Production

RUN apt-get update && apt-get install -y wget && rm -rf /var/lib/apt/lists/*
ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

#COPY ssl/localhost+2.pem /app
#COPY ssl/localhost+2-key.pem /app
#RUN chmod 644 /app/localhost+2.pem /app/localhost+2-key.pem

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["S2Api.csproj", "."]
RUN dotnet restore "./S2Api.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "S2Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "S2Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "S2Api.dll"]
# ENTRYPOINT ["dotnet", "S2Api.dll", "--server.urls", "https://+:443", "--cert", "/app/localhost+2.pem", "--key", "/app/localhost+2-key.pem"]
