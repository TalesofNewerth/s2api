using System;
using S2Api.Enums;

namespace S2Api.Utils;

public static class AffixUtils
{
    public static int GetCost(int affixId, RuneStageEnum stageId)
    {
        return stageId switch
        {
            RuneStageEnum.Type => // Type
                GetTypeCost(affixId),
            RuneStageEnum.Color => // Regen
                GetRegenCost(affixId),
            RuneStageEnum.Passive => // Passive
                GetPassiveCost(affixId),
            RuneStageEnum.Active => // Active
                GetActiveCost(affixId),
            _ => throw new ArgumentException("Invalid stage id.")
        };
    }

    private static int GetTypeCost(int affixId)
    {
        return (RuneTypeEnum)affixId switch
        {
            RuneTypeEnum.Ring => // Ring
                25, //used to be 5, temp mesure.
            /*RuneTypeEnum.Amulet => // Amulet
                10,
            RuneTypeEnum.Jewel => // Jewel
                15,
            RuneTypeEnum.Rune => // Rune
                25,*/
            _ => throw new ArgumentException("Invalid type affix id.")
        };
    }

    private static int GetRegenCost(int affixId)
    {
        return (RuneColorEnum)affixId switch
        {
            RuneColorEnum.Red => // Red
                5,
            RuneColorEnum.Blue => // Blue
                10,
            RuneColorEnum.White => // White
                15,
            _ => throw new ArgumentException("Invalid regen affix id.")
        };
    }

    private static int GetPassiveCost(int affixId)
    {
        return (RunePassiveEnum)affixId switch
        {
            RunePassiveEnum.Dolphin => // Dolphin
                15,
            RunePassiveEnum.Beaver => // Beaver
                10,
            RunePassiveEnum.Armadillo => // Armadillo
                20,
            RunePassiveEnum.Bear => // Bear
                20,
            RunePassiveEnum.Rabbit => // Rabbit
                10,
            _ => throw new ArgumentException("Invalid passive affix id.")
        };
    }

    private static int GetActiveCost(int affixId)
    {
        return (RuneActiveEnum)affixId switch
        {
            RuneActiveEnum.Lung => // Lungs
                10,
            RuneActiveEnum.Heart => // Heart
                15,
            RuneActiveEnum.Shield => // Shield
                15,
            RuneActiveEnum.Brain => // Brain
                10,
            RuneActiveEnum.Dagger => // Dagger
                25,
            _ => throw new ArgumentException("Invalid active affix id.")
        };
    }
}