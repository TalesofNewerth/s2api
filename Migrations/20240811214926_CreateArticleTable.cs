﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace S2Api.Migrations
{
    public partial class CreateArticleTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "playerinfos");

            migrationBuilder.DropTable(
                name: "playerstats");

            migrationBuilder.DropColumn(
                name: "item_id",
                table: "stored_items");

            migrationBuilder.DropColumn(
                name: "mod_stage",
                table: "stored_items");

            migrationBuilder.DropColumn(
                name: "item_id",
                table: "items");

            migrationBuilder.AlterColumn<int>(
                name: "end_status",
                table: "commanders",
                type: "int(11)",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int(11)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "launcher_updates",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    version = table.Column<string>(type: "longtext", nullable: true, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    notes = table.Column<string>(type: "longtext", nullable: true, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    pub_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    platforms_json = table.Column<string>(type: "text", nullable: true, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_launcher_updates", x => x.id);
                })
                .Annotation("MySql:CharSet", "latin1")
                .Annotation("Relational:Collation", "latin1_swedish_ci");

            migrationBuilder.CreateTable(
                name: "LauncherArticles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Md = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    Type = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    Title = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    Thumb = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    Authors = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    Release = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    Category = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LauncherArticles", x => x.Id);
                })
                .Annotation("MySql:CharSet", "latin1")
                .Annotation("Relational:Collation", "latin1_swedish_ci");

            migrationBuilder.CreateTable(
                name: "releases",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "longtext", nullable: false, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    tag_name = table.Column<string>(type: "longtext", nullable: false, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    description = table.Column<string>(type: "longtext", nullable: true, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    created_at = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    published_at = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    is_draft = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    is_prerelease = table.Column<bool>(type: "tinyint(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_releases", x => x.id);
                })
                .Annotation("MySql:CharSet", "latin1")
                .Annotation("Relational:Collation", "latin1_swedish_ci");

            migrationBuilder.CreateTable(
                name: "assets",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "longtext", nullable: false, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    content_type = table.Column<string>(type: "longtext", nullable: false, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    size = table.Column<long>(type: "bigint", nullable: false),
                    download_url = table.Column<string>(type: "longtext", nullable: false, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    release_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_assets", x => x.id);
                    table.ForeignKey(
                        name: "fk_asset_release",
                        column: x => x.release_id,
                        principalTable: "releases",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "latin1")
                .Annotation("Relational:Collation", "latin1_swedish_ci");

            migrationBuilder.CreateIndex(
                name: "IX_assets_release_id",
                table: "assets",
                column: "release_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "assets");

            migrationBuilder.DropTable(
                name: "launcher_updates");

            migrationBuilder.DropTable(
                name: "LauncherArticles");

            migrationBuilder.DropTable(
                name: "releases");

            migrationBuilder.AddColumn<int>(
                name: "item_id",
                table: "stored_items",
                type: "int(11)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "mod_stage",
                table: "stored_items",
                type: "int(11)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "item_id",
                table: "items",
                type: "int(11)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "end_status",
                table: "commanders",
                type: "int(11)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int(11)");

            migrationBuilder.CreateTable(
                name: "playerinfos",
                columns: table => new
                {
                    account_id = table.Column<int>(type: "int(11)", nullable: false),
                    ap = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    clan_id = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    karma = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    level = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    lf = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    overall_r = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    sf = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'90'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.account_id);
                    table.ForeignKey(
                        name: "FK_playersinfos_account",
                        column: x => x.account_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "latin1")
                .Annotation("Relational:Collation", "latin1_swedish_ci");

            migrationBuilder.CreateTable(
                name: "playerstats",
                columns: table => new
                {
                    account_id = table.Column<int>(type: "int(11)", nullable: false),
                    assists = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    bdmg = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    d_conns = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    deaths = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    devourer = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    earned_exp = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    exp = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    gold = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    hp_healed = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    hp_repaired = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    kills = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    losses = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    malphas = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    npc = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    pdmg = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    razed = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    res = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    revenant = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    secs = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    souls = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    ties = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    total_secs = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'"),
                    wins = table.Column<int>(type: "int(11)", nullable: true, defaultValueSql: "'0'")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.account_id);
                })
                .Annotation("MySql:CharSet", "latin1")
                .Annotation("Relational:Collation", "latin1_swedish_ci");

            migrationBuilder.CreateIndex(
                name: "FK_ClanPlayerInfo",
                table: "playerinfos",
                column: "clan_id");
        }
    }
}
