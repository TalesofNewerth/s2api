-- Create email_confirmation_tokens Table
CREATE TABLE email_confirmation_tokens (
    id INT AUTO_INCREMENT PRIMARY KEY,
    token VARCHAR(256) NOT NULL,
    user_id INT NOT NULL,
    created_at DATETIME NOT NULL,
    expiration_date DATETIME NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);

-- Create refresh_tokens Table
CREATE TABLE refresh_tokens (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    token LONGTEXT,
    expiration_date DATETIME(6) NOT NULL,
    created_at DATETIME(6) NOT NULL,
    revoked TINYINT(1) NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id)
);

-- Create user_stashes Table
CREATE TABLE user_stashes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    account_id INT NOT NULL,
    gold INT NOT NULL,
    FOREIGN KEY (account_id) REFERENCES users (id) UNIQUE
);

-- Create stored_items Table
CREATE TABLE stored_items (
    id INT AUTO_INCREMENT PRIMARY KEY,
    stash_id INT,
    item_id INT,
    type INT,
    creation_date DATETIME(6),
    mod_stage INT(11),
    FOREIGN KEY (stash_id) REFERENCES user_stashes (id)
);

-- Add columns to users Table
ALTER TABLE users
ADD email_confirmed BOOLEAN NOT NULL DEFAULT 0,
ADD last_verification_email_sent_at DATETIME,
ADD reset_password_token LONGTEXT,
ADD reset_password_expiration DATETIME;

-- Add mod_stage column to stored_items Table
ALTER TABLE stored_items
ADD mod_stage INT(11);

-- Insert data into user_stashes
INSERT INTO user_stashes (account_id, gold)
SELECT id, 0 FROM users;

CREATE VIEW playerstats as
SELECT a.account_id, sum(a.exp) as exp, SUM(a.kills) as kills, SUM(a.assists) as assists, SUM(a.souls) as souls, SUM(a.razed) as razed, 
	SUM(a.pdmg) as pdmg, SUM(a.bdmg) as bdmg, SUM(a.npc) as npc, 
	SUM(a.hp_healed) as hp_healed, SUM(a.res) as res, SUM(a.gold) as gold, SUM(a.hp_repaired) as hp_repaired, SUM(a.secs) AS secs, 
	SUM(m.winner = cast(t.race as int)) as wins, 
	SUM(m.winner != cast(t.race as int)) as losses,
	SUM(a.end_status = 0) AS d_conns,
	0 as ties
from actionplayers a join teams t on a.team_id = t.id join matches m on t.match = m.id
group by a.account_id

CREATE VIEW playerinfos AS
SELECT 
    a.account_id AS account_id,
    ROUND(
        (
            SUM(m.winner = CAST(t.race AS SIGNED)) / COUNT(a.account_id) * 100 * 2.5 
            + (SUM(a.exp) / (SUM(a.secs) / 60)*4)
        ) / 5,
        0
    ) AS sf,
    ROW_NUMBER() OVER(ORDER BY sf desc) overall_r,
    0 AS lf,
    0 AS level,
    0 AS karma,
    0 AS clan_id 
FROM 
    masterserver.actionplayers a 
JOIN 
    masterserver.match_summs s ON a.match_id = s.id
JOIN 
    masterserver.teams t ON a.team_id = t.id
JOIN 
    masterserver.matches m ON t.match = m.id 
WHERE 
    DATEDIFF(CURRENT_DATE, s.created_at) <= 90
GROUP BY 
    a.account_id 
HAVING 
    COUNT(a.account_id) > 5;

ALTER TABLE items DROP COLUMN item_id;

