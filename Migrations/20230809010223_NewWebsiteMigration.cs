﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace S2Api.Migrations
{
    public partial class NewWebsiteMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "email_confirmation_tokens",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    token = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: false, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    user_id = table.Column<int>(type: "int(11)", nullable: false),
                    created_at = table.Column<DateTime>(type: "timestamp", nullable: false),
                    expiration_date = table.Column<DateTime>(type: "timestamp", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_email_confirmation_tokens", x => x.id);
                    table.ForeignKey(
                        name: "FK_email_confirmation_tokens_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "latin1")
                .Annotation("Relational:Collation", "latin1_swedish_ci");

            migrationBuilder.CreateTable(
                name: "refresh_tokens",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<int>(type: "int(11)", nullable: false),
                    token = table.Column<string>(type: "longtext", nullable: true, collation: "latin1_swedish_ci")
                        .Annotation("MySql:CharSet", "latin1"),
                    expiration_date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    revoked = table.Column<bool>(type: "tinyint(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_refresh_tokens", x => x.id);
                    table.ForeignKey(
                        name: "FK_refresh_tokens_users",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id");
                })
                .Annotation("MySql:CharSet", "latin1")
                .Annotation("Relational:Collation", "latin1_swedish_ci");

            migrationBuilder.CreateTable(
                name: "user_stashes",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    account_id = table.Column<int>(type: "int(11)", nullable: false),
                    gold = table.Column<int>(type: "int(11)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_stash", x => x.id);
                    table.ForeignKey(
                        name: "user_stash_account_FK",
                        column: x => x.account_id,
                        principalTable: "users",
                        principalColumn: "id");
                })
                .Annotation("MySql:CharSet", "latin1")
                .Annotation("Relational:Collation", "latin1_swedish_ci");

            migrationBuilder.CreateTable(
                name: "stored_items",
                columns: table => new
                {
                    id = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    stash_id = table.Column<int>(type: "int(11)", nullable: true),
                    item_id = table.Column<int>(type: "int(11)", nullable: true),
                    type = table.Column<int>(type: "int(11)", nullable: true),
                    creation_date = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    mod_stage = table.Column<int>(type: "int(11)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stored_items", x => x.id);
                    table.ForeignKey(
                        name: "stored_items_stash_FK",
                        column: x => x.stash_id,
                        principalTable: "user_stashes",
                        principalColumn: "id");
                })
                .Annotation("MySql:CharSet", "latin1")
                .Annotation("Relational:Collation", "latin1_swedish_ci");

            migrationBuilder.CreateIndex(
                name: "FK_email_confirmation_tokens_users",
                table: "email_confirmation_tokens",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_refresh_tokens_user_id",
                table: "refresh_tokens",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "stored_items_stash_FK",
                table: "stored_items",
                column: "stash_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_stash_account_id",
                table: "user_stashes",
                column: "account_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "user_stash_account_FK",
                table: "user_stashes",
                column: "account_id");

            // Add EmailConfirmed column to users table
            migrationBuilder.AddColumn<bool>(
                name: "email_confirmed",
                table: "users",
                type: "bool",
                nullable: false,
                defaultValue: false);

            // Add LastVerificationEmail_sent_at column to users table
            migrationBuilder.AddColumn<DateTime>(
                name: "last_verification_email_sent_at",
                table: "users",
                type: "datetime",
                nullable: true);

            // Add ResetPasswordToken column to users table
            migrationBuilder.AddColumn<string>(
                name: "reset_password_token",
                table: "users",
                type: "longtext",
                nullable: true);

            // Add ResetPasswordExpiration column to users table
            migrationBuilder.AddColumn<DateTime>(
                name: "reset_password_expiration",
                table: "users",
                type: "datetime",
                nullable: true);

            // Add ResetPasswordExpiration column to users table
            migrationBuilder.AddColumn<DateTime>(
                name: "mod_stage",
                table: "stored_items",
                type: "int(11)",
                nullable: true);

            migrationBuilder.Sql("INSERT INTO user_stashes (account_id, gold) SELECT id, 0 FROM users");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // Drop UserStash column from users table
            migrationBuilder.DropTable(
                name: "user_stashes");

            // Drop StoredItems column from users table
            migrationBuilder.DropTable(
                name: "stored_items");

            // Drop ResetPasswordToken column from users table
            migrationBuilder.DropColumn(
                name: "reset_password_token",
                table: "users");

            // Drop ResetPasswordToken column from users table
            migrationBuilder.DropColumn(
                name: "reset_password_token",
                table: "users");

            // Drop ResetPasswordExpiration column from users table
            migrationBuilder.DropColumn(
                name: "reset_password_expiration",
                table: "users");

            // Drop RefreshTokens table
            migrationBuilder.DropTable(
                name: "refresh_tokens");

            // Drop EmailConfirmationTokens table
            migrationBuilder.DropTable(
                name: "email_confirmation_tokens");

            // Drop EmailConfirmed column from users table
            migrationBuilder.DropColumn(
                name: "email_confirmed",
                table: "users");

            // Drop EmailConfirmed column from users table
            migrationBuilder.DropColumn(
                name: "mod_stage",
                table: "stored_items");

            migrationBuilder.Sql("DELETE FROM user_stashes");
        }
    }
}
