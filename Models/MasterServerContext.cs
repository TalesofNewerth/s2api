﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using S2Api.Models.MasterServer;
using S2Api.Models.Authentication;
using S2Api.Models.Launcher;
using System.Linq;

namespace S2Api.Models
{
    public partial class MasterServerContext : DbContext
    {
        public MasterServerContext(DbContextOptions<MasterServerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Achievement> Achievements { get; set; }
        public virtual DbSet<Actionplayer> Actionplayers { get; set; }
        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<Badge> Badges { get; set; }
        public virtual DbSet<Ban> Bans { get; set; }
        public virtual DbSet<Buddy> Buddies { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Clan> Clans { get; set; }
        public virtual DbSet<Commander> Commanders { get; set; }
        public virtual DbSet<Commanderstat> Commanderstats { get; set; }
        public virtual DbSet<Download> Downloads { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<Karma> Karmas { get; set; }
        public virtual DbSet<Map> Maps { get; set; }
        public virtual DbSet<Match> Matches { get; set; }
        public virtual DbSet<MatchSumm> MatchSumms { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<Playerinfo> Playerinfos { get; set; }
        public virtual DbSet<Playerstat> Playerstats { get; set; }
        public virtual DbSet<Server> Servers { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Vote> Votes { get; set; }
        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }
        public virtual DbSet<EmailConfirmationToken> EmailConfirmationTokens { get; set; }

        public virtual DbSet<UserStash> UserStashes { get; set; }
        public virtual DbSet<StoredItem> StoredItems { get; set; }

        // Releases API
        public DbSet<Release> Releases { get; set; }
        public DbSet<Asset> Assets { get; set; }

        // Launcher Updates API
        public DbSet<LauncherUpdate> LauncherUpdates { get; set; }

        // Launcher News API
        public DbSet<LauncherArticle> LauncherArticles { get; set;}
        public DbSet<LauncherArticleContent> LauncherArticleContent { get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("latin1_swedish_ci")
                .HasCharSet("latin1");

            modelBuilder.Entity<Achievement>(entity =>
            {
                entity.ToTable("achievements");

                entity.HasIndex(e => e.Id, "id");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Condition)
                    .HasMaxLength(255)
                    .HasColumnName("condition")
                    .HasDefaultValueSql("'NONE'");

                entity.Property(e => e.Description)
                    .HasColumnType("text")
                    .HasColumnName("description");

                entity.Property(e => e.Name)
                    .HasMaxLength(255)
                    .HasColumnName("name");

                entity.Property(e => e.Points)
                    .HasColumnType("int(25)")
                    .HasColumnName("points");

                entity.Property(e => e.Style)
                    .HasMaxLength(255)
                    .HasColumnName("style");

                entity.Property(e => e.Value)
                    .HasColumnType("int(11)")
                    .HasColumnName("value")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<Actionplayer>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.MatchId, e.TeamId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.ToTable("actionplayers");

                entity.HasIndex(e => e.TeamId, "FK_actionplayers_");

                entity.HasIndex(e => e.MatchId, "match");

                entity.HasIndex(e => new { e.MatchId, e.TeamId }, "match_2");

                entity.HasIndex(e => e.AccountId, "user");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .HasColumnName("account_id");

                entity.Property(e => e.MatchId)
                    .HasColumnType("int(11)")
                    .HasColumnName("match_id");

                entity.Property(e => e.TeamId)
                    .HasColumnType("int(11)")
                    .HasColumnName("team_id");

                entity.Property(e => e.Assists)
                    .HasColumnType("int(11)")
                    .HasColumnName("assists");

                entity.Property(e => e.Bdmg)
                    .HasColumnType("int(11)")
                    .HasColumnName("bdmg");

                entity.Property(e => e.Deaths)
                    .HasColumnType("int(11)")
                    .HasColumnName("deaths");

                entity.Property(e => e.EndStatus)
                    .HasColumnType("int(11)")
                    .HasColumnName("end_status");

                entity.Property(e => e.Exp)
                    .HasColumnType("int(11)")
                    .HasColumnName("exp");

                entity.Property(e => e.Gold)
                    .HasColumnType("int(11)")
                    .HasColumnName("gold");

                entity.Property(e => e.HpHealed)
                    .HasColumnType("int(11)")
                    .HasColumnName("hp_healed");

                entity.Property(e => e.HpRepaired)
                    .HasColumnType("int(11)")
                    .HasColumnName("hp_repaired");

                entity.Property(e => e.Ip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("ip");

                entity.Property(e => e.Kills)
                    .HasColumnType("int(11)")
                    .HasColumnName("kills");

                entity.Property(e => e.Npc)
                    .HasColumnType("int(11)")
                    .HasColumnName("npc");

                entity.Property(e => e.Pdmg)
                    .HasColumnType("int(11)")
                    .HasColumnName("pdmg");

                entity.Property(e => e.Razed)
                    .HasColumnType("int(11)")
                    .HasColumnName("razed");

                entity.Property(e => e.Res)
                    .HasColumnType("int(11)")
                    .HasColumnName("res");

                entity.Property(e => e.Secs)
                    .HasColumnType("int(11)")
                    .HasColumnName("secs");

                entity.Property(e => e.Sf)
                    .HasColumnType("int(11)")
                    .HasColumnName("sf");

                entity.Property(e => e.Souls)
                    .HasColumnType("int(11)")
                    .HasColumnName("souls");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Actionplayers)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_actionplayers_user");

                entity.HasOne(d => d.Match)
                    .WithMany(p => p.Actionplayers)
                    .HasForeignKey(d => d.MatchId)
                    .HasConstraintName("FK_actionplayers_matches");

                entity.HasOne(d => d.Team)
                    .WithMany(p => p.Actionplayers)
                    .HasForeignKey(d => d.TeamId)
                    .HasConstraintName("FK_actionplayers_");
            });

            modelBuilder.Entity<Article>(entity =>
            {
                entity.ToTable("articles");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.AccountId, "FK_ArticlesUser");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .HasColumnName("account_id");

                entity.Property(e => e.Content)
                    .HasColumnType("text")
                    .HasColumnName("content");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("timestamp")
                    .HasColumnName("created_at");

                entity.Property(e => e.ImageId)
                    .HasColumnType("int(11)")
                    .HasColumnName("image_id");

                entity.Property(e => e.Title)
                    .HasMaxLength(50)
                    .HasColumnName("title");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnType("timestamp")
                    .HasColumnName("updated_at");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Articles)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_ArticlesUser");
            });

            modelBuilder.Entity<Badge>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.AchievementId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("badges");

                entity.HasIndex(e => e.AccountId, "FK_badgesUsers");

                entity.HasIndex(e => e.AchievementId, "FK_badges_achivements");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .HasColumnName("account_id");

                entity.Property(e => e.AchievementId)
                    .HasColumnType("int(11)")
                    .HasColumnName("achievement_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Badges)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_badgesUsers");

                entity.HasOne(d => d.Achievement)
                    .WithMany(p => p.Badges)
                    .HasForeignKey(d => d.AchievementId)
                    .HasConstraintName("FK_badges_achivements");
            });

            modelBuilder.Entity<Ban>(entity =>
            {
                entity.ToTable("bans");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.AccountId, "FK_bansUsers");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(10)")
                    .HasColumnName("account_id");

                entity.Property(e => e.Banneduntil)
                    .HasColumnType("datetime")
                    .HasColumnName("banneduntil");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Bans)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_bansUsers");
            });

            modelBuilder.Entity<Buddy>(entity =>
            {
                entity.HasKey(e => new { e.SourceId, e.TargetId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("buddies");

                entity.HasIndex(e => e.TargetId, "buddies_target_FK");

                entity.Property(e => e.SourceId)
                    .HasColumnType("int(11)")
                    .HasColumnName("source_id");

                entity.Property(e => e.TargetId)
                    .HasColumnType("int(11)")
                    .HasColumnName("target_id");

                entity.Property(e => e.Avatar)
                    .HasMaxLength(50)
                    .HasColumnName("avatar");

                entity.Property(e => e.ClanImg)
                    .HasMaxLength(50)
                    .HasColumnName("clan_img");

                entity.Property(e => e.ClanName)
                    .HasMaxLength(50)
                    .HasColumnName("clan_name");

                entity.Property(e => e.ClanTag)
                    .HasMaxLength(50)
                    .HasColumnName("clan_tag");

                entity.Property(e => e.Note)
                    .HasMaxLength(50)
                    .HasColumnName("note");

                entity.HasOne(d => d.Source)
                    .WithMany(p => p.BuddySources)
                    .HasForeignKey(d => d.SourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("buddies_source_FK");

                entity.HasOne(d => d.Target)
                    .WithMany(p => p.BuddyTargets)
                    .HasForeignKey(d => d.TargetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("buddies_target_FK");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("categories");

                entity.HasIndex(e => e.ImageId, "categories_image_FK");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.ImageId)
                    .HasColumnType("int(11)")
                    .HasColumnName("image_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("name");

                entity.HasOne(d => d.Image)
                    .WithMany(p => p.Categories)
                    .HasForeignKey(d => d.ImageId)
                    .HasConstraintName("categories_image_FK");
            });

            modelBuilder.Entity<Clan>(entity =>
            {
                entity.ToTable("clans");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.ClanImg)
                    .HasMaxLength(20)
                    .HasColumnName("clan_img");

                entity.Property(e => e.ClanName)
                    .HasMaxLength(20)
                    .HasColumnName("clan_name");

                entity.Property(e => e.ClanTag)
                    .HasMaxLength(3)
                    .HasColumnName("clan_tag");
            });

            modelBuilder.Entity<Commander>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.MatchId, e.TeamId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.ToTable("commanders");

                entity.HasIndex(e => e.TeamId, "FK_commanders_teams");

                entity.HasIndex(e => e.MatchId, "match");

                entity.HasIndex(e => new { e.MatchId, e.TeamId }, "match_2");

                entity.HasIndex(e => e.AccountId, "user");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .HasColumnName("account_id");

                entity.Property(e => e.MatchId)
                    .HasColumnType("int(11)")
                    .HasColumnName("match_id");

                entity.Property(e => e.TeamId)
                    .HasColumnType("int(11)")
                    .HasColumnName("team_id");

                entity.Property(e => e.Buffs)
                    .HasColumnType("int(11)")
                    .HasColumnName("buffs");

                entity.Property(e => e.Builds)
                    .HasColumnType("int(11)")
                    .HasColumnName("builds");

                entity.Property(e => e.Debuffs)
                    .HasColumnType("int(11)")
                    .HasColumnName("debuffs");

                entity.Property(e => e.EndStatus)
                    .HasColumnType("int(11)")
                    .HasColumnName("end_status");

                entity.Property(e => e.Exp)
                    .HasColumnType("int(11)")
                    .HasColumnName("exp");

                entity.Property(e => e.Gold)
                    .HasColumnType("int(11)")
                    .HasColumnName("gold");

                entity.Property(e => e.HpHealed)
                    .HasColumnType("int(11)")
                    .HasColumnName("hp_healed");

                entity.Property(e => e.Ip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("ip");

                entity.Property(e => e.Kills)
                    .HasColumnType("int(11)")
                    .HasColumnName("kills");

                entity.Property(e => e.Orders)
                    .HasColumnType("int(11)")
                    .HasColumnName("orders");

                entity.Property(e => e.Pdmg)
                    .HasColumnType("int(11)")
                    .HasColumnName("pdmg");

                entity.Property(e => e.Razed)
                    .HasColumnType("int(11)")
                    .HasColumnName("razed");

                entity.Property(e => e.Secs)
                    .HasColumnType("int(11)")
                    .HasColumnName("secs");

                entity.Property(e => e.Sf)
                    .HasColumnType("int(11)")
                    .HasColumnName("sf");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Commanders)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_commanders_account");

                entity.HasOne(d => d.Match)
                    .WithMany(p => p.Commanders)
                    .HasForeignKey(d => d.MatchId)
                    .HasConstraintName("FK_commanders_match");

                entity.HasOne(d => d.Team)
                    .WithMany(p => p.Commanders)
                    .HasForeignKey(d => d.TeamId)
                    .HasConstraintName("FK_commanders_teams");
            });

            modelBuilder.Entity<Commanderstat>(entity =>
            {
                entity.HasKey(e => e.AccountId)
                    .HasName("PRIMARY");

                entity.ToTable("commanderstats");

                entity.HasIndex(e => e.AccountId, "FK_UsersCommanderStats");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever()
                    .HasColumnName("account_id");

                entity.Property(e => e.Assists)
                    .HasColumnType("int(11)")
                    .HasColumnName("assists")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Buffs)
                    .HasColumnType("int(11)")
                    .HasColumnName("buffs")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Builds)
                    .HasColumnType("int(11)")
                    .HasColumnName("builds")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DConns)
                    .HasColumnType("int(11)")
                    .HasColumnName("d_conns")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Debuffs)
                    .HasColumnType("int(11)")
                    .HasColumnName("debuffs")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.EarnedExp)
                    .HasColumnType("int(11)")
                    .HasColumnName("earned_exp")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnType("int(11)")
                    .HasColumnName("exp")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Gold)
                    .HasColumnType("int(11)")
                    .HasColumnName("gold")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.HpHealed)
                    .HasColumnType("int(11)")
                    .HasColumnName("hp_healed")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Kills)
                    .HasColumnType("int(11)")
                    .HasColumnName("kills")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Losses)
                    .HasColumnType("int(11)")
                    .HasColumnName("losses")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Orders)
                    .HasColumnType("int(11)")
                    .HasColumnName("orders")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Pdmg)
                    .HasColumnType("int(11)")
                    .HasColumnName("pdmg")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Razed)
                    .HasColumnType("int(11)")
                    .HasColumnName("razed")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Secs)
                    .HasColumnType("int(11)")
                    .HasColumnName("secs")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Wins)
                    .HasColumnType("int(11)")
                    .HasColumnName("wins")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Winstreak)
                    .HasColumnType("int(11)")
                    .HasColumnName("winstreak")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<Download>(entity =>
            {
                entity.ToTable("downloads");

                entity.HasIndex(e => e.CategorieId, "FK_downloadCategorie");

                entity.HasIndex(e => e.ImageId, "downloads_image_FK");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.CategorieId)
                    .HasColumnType("int(11)")
                    .HasColumnName("categorie_id");

                entity.Property(e => e.Description)
                    .HasColumnType("tinytext")
                    .HasColumnName("description");

                entity.Property(e => e.ImageId)
                    .HasColumnType("int(11)")
                    .HasColumnName("image_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("name");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("url");

                entity.HasOne(d => d.Categorie)
                    .WithMany(p => p.Downloads)
                    .HasForeignKey(d => d.CategorieId)
                    .HasConstraintName("downloads_category_FK");

                entity.HasOne(d => d.Image)
                    .WithMany(p => p.Downloads)
                    .HasForeignKey(d => d.ImageId)
                    .HasConstraintName("downloads_image_FK");
            });

            modelBuilder.Entity<Image>(entity =>
            {
                entity.ToTable("images");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.ImageUrl)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("image_url");
            });

            modelBuilder.Entity<Item>(entity =>
            {
                entity.ToTable("items");

                entity.HasIndex(e => e.AccountId, "items_account_FK");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .HasColumnName("account_id");

                entity.Property(e => e.ExpDate).HasColumnName("exp_date");

                entity.Property(e => e.Type)
                    .HasColumnType("int(11)")
                    .HasColumnName("type");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Items)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("items_account_FK");
            });

            modelBuilder.Entity<Karma>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.TargetId, e.MatchId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.ToTable("karmas");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.HasIndex(e => e.MatchId, "FK_KarmasMatch");

                entity.HasIndex(e => e.TargetId, "FK_KarmasTarget");

                entity.HasIndex(e => e.AccountId, "FK_karmasAccount");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .HasColumnName("account_id");

                entity.Property(e => e.TargetId)
                    .HasColumnType("int(11)")
                    .HasColumnName("target_id");

                entity.Property(e => e.MatchId)
                    .HasColumnType("int(11)")
                    .HasColumnName("match_id");

                entity.Property(e => e.Do)
                    .HasMaxLength(50)
                    .HasColumnName("do");

                entity.Property(e => e.Reason)
                    .HasColumnType("text")
                    .HasColumnName("reason");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.KarmaAccounts)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("FK_karmasAccount");

                entity.HasOne(d => d.Match)
                    .WithMany(p => p.Karmas)
                    .HasForeignKey(d => d.MatchId)
                    .HasConstraintName("FK_KarmasMatch");

                entity.HasOne(d => d.Target)
                    .WithMany(p => p.KarmaTargets)
                    .HasForeignKey(d => d.TargetId)
                    .HasConstraintName("FK_KarmasTarget");
            });

            modelBuilder.Entity<Map>(entity =>
            {
                entity.ToTable("maps");

                entity.HasIndex(e => e.Name, "name")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Match>(entity =>
            {
                entity.ToTable("matches");

                entity.HasIndex(e => e.Map, "map");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Duration)
                    .HasColumnType("time")
                    .HasColumnName("duration");

                entity.Property(e => e.Map)
                    .IsRequired()
                    .HasMaxLength(40)
                    .HasColumnName("map");

                entity.Property(e => e.Winner)
                    .HasColumnType("int(11)")
                    .HasColumnName("winner");
            });

            modelBuilder.Entity<MatchSumm>(entity =>
            {
                entity.ToTable("match_summs");

                entity.HasCharSet("utf8mb4")
                    .UseCollation("utf8mb4_general_ci");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasColumnName("created_at");

                entity.Property(e => e.Map)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("map");

                entity.Property(e => e.Port)
                    .HasColumnType("int(11)")
                    .HasColumnName("port");

                entity.Property(e => e.ServerId)
                    .HasColumnType("int(11)")
                    .HasColumnName("server_id");
            });

            modelBuilder.Entity<Player>(entity =>
            {
                entity.HasKey(e => e.AccountId)
                    .HasName("PRIMARY");

                entity.ToTable("players");

                entity.HasIndex(e => e.Server, "players_servers_FK");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever()
                    .HasColumnName("account_id");

                entity.Property(e => e.Online)
                    .HasColumnType("int(11)")
                    .HasColumnName("online")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Server)
                    .HasColumnType("int(11)")
                    .HasColumnName("server")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Updated)
                    .HasColumnType("datetime")
                    .HasColumnName("updated");

                entity.HasOne(d => d.Account)
                    .WithOne(p => p.Player)
                    .HasForeignKey<Player>(d => d.AccountId)
                    .HasConstraintName("players_users_FK");

                entity.HasOne(d => d.ServerNavigation)
                    .WithMany(p => p.Players)
                    .HasForeignKey(d => d.Server)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("players_servers_FK");
            });

            modelBuilder.Entity<Playerinfo>(entity =>
            {
                entity.HasKey(e => e.AccountId)
                    .HasName("PRIMARY");

                entity.ToView("playerinfos");

                entity.HasIndex(e => e.ClanId, "FK_ClanPlayerInfo");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever()
                    .HasColumnName("account_id");

                entity.Property(e => e.Ap)
                    .HasColumnType("int(11)")
                    .HasColumnName("ap")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ClanId)
                    .HasColumnType("int(11)")
                    .HasColumnName("clan_id")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Karma)
                    .HasColumnType("int(11)")
                    .HasColumnName("karma")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Level)
                    .HasColumnType("int(11)")
                    .HasColumnName("level")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Lf)
                    .HasColumnType("int(11)")
                    .HasColumnName("lf")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.OverallR)
                    .HasColumnType("int(11)")
                    .HasColumnName("overall_r")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Sf)
                    .HasColumnType("int(11)")
                    .HasColumnName("sf")
                    .HasDefaultValueSql("'90'");

                entity.HasOne(d => d.Account)
                    .WithOne(p => p.Playerinfo)
                    .HasForeignKey<Playerinfo>(d => d.AccountId)
                    .HasConstraintName("FK_playersinfos_account");
            });

            modelBuilder.Entity<Playerstat>(entity =>
            {
                entity.HasKey(e => e.AccountId)
                    .HasName("PRIMARY");

                entity.ToView("playerstats");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever()
                    .HasColumnName("account_id");

                entity.Property(e => e.Assists)
                    .HasColumnType("int(11)")
                    .HasColumnName("assists")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Bdmg)
                    .HasColumnType("int(11)")
                    .HasColumnName("bdmg")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.DConns)
                    .HasColumnType("int(11)")
                    .HasColumnName("d_conns")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Deaths)
                    .HasColumnType("int(11)")
                    .HasColumnName("deaths")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Devourer)
                    .HasColumnType("int(11)")
                    .HasColumnName("devourer")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Exp)
                    .HasColumnType("int(11)")
                    .HasColumnName("exp")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Gold)
                    .HasColumnType("int(11)")
                    .HasColumnName("gold")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.HpHealed)
                    .HasColumnType("int(11)")
                    .HasColumnName("hp_healed")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.HpRepaired)
                    .HasColumnType("int(11)")
                    .HasColumnName("hp_repaired")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Kills)
                    .HasColumnType("int(11)")
                    .HasColumnName("kills")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Losses)
                    .HasColumnType("int(11)")
                    .HasColumnName("losses")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Malphas)
                    .HasColumnType("int(11)")
                    .HasColumnName("malphas")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Npc)
                    .HasColumnType("int(11)")
                    .HasColumnName("npc")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Pdmg)
                    .HasColumnType("int(11)")
                    .HasColumnName("pdmg")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Razed)
                    .HasColumnType("int(11)")
                    .HasColumnName("razed")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Res)
                    .HasColumnType("int(11)")
                    .HasColumnName("res")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Revenant)
                    .HasColumnType("int(11)")
                    .HasColumnName("revenant")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Secs)
                    .HasColumnType("int(11)")
                    .HasColumnName("secs")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Souls)
                    .HasColumnType("int(11)")
                    .HasColumnName("souls")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Ties)
                    .HasColumnType("int(11)")
                    .HasColumnName("ties")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Wins)
                    .HasColumnType("int(11)")
                    .HasColumnName("wins")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<Server>(entity =>
            {
                entity.ToTable("servers");

                entity.HasIndex(e => new { e.Ip, e.Port }, "ipport")
                    .IsUnique();

                entity.HasIndex(e => e.Login, "login")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Description)
                    .HasColumnType("text")
                    .HasColumnName("description");

                entity.Property(e => e.Ip)
                    .HasMaxLength(32)
                    .HasColumnName("ip");

                entity.Property(e => e.Login)
                    .HasMaxLength(50)
                    .HasColumnName("login");

                entity.Property(e => e.Map)
                    .HasMaxLength(75)
                    .HasColumnName("map");

                entity.Property(e => e.MaxConn)
                    .HasColumnType("int(11)")
                    .HasColumnName("max_conn");

                entity.Property(e => e.Maxlevel)
                    .HasColumnType("int(11)")
                    .HasColumnName("maxlevel");

                entity.Property(e => e.Minlevel)
                    .HasColumnType("int(11)")
                    .HasColumnName("minlevel");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");

                entity.Property(e => e.NextMap)
                    .HasMaxLength(75)
                    .HasColumnName("next_map");

                entity.Property(e => e.NumConn)
                    .HasColumnType("int(11)")
                    .HasColumnName("num_conn");

                entity.Property(e => e.Official)
                    .IsRequired()
                    .HasMaxLength(1)
                    .HasColumnName("official")
                    .HasDefaultValueSql("'0'")
                    .IsFixedLength();

                entity.Property(e => e.Online)
                    .HasColumnName("online")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Port)
                    .HasColumnType("int(11)")
                    .HasColumnName("port");

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .HasColumnName("status");

                entity.Property(e => e.Updated)
                    .HasColumnType("datetime")
                    .HasColumnName("updated");
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.ToTable("teams");

                entity.HasIndex(e => e.Match, "match");

                entity.HasIndex(e => new { e.Match, e.Race }, "matchrace")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.AvgSf)
                    .HasColumnType("int(11)")
                    .HasColumnName("avg_sf");

                entity.Property(e => e.Commander)
                    .HasMaxLength(50)
                    .HasColumnName("commander");

                entity.Property(e => e.Match)
                    .HasColumnType("int(11)")
                    .HasColumnName("match");

                entity.Property(e => e.Race)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("race");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Cookie)
                    .HasMaxLength(64)
                    .HasColumnName("cookie");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("timestamp")
                    .HasColumnName("created_at");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.Password)
                    .HasMaxLength(128)
                    .HasColumnName("password");

                entity.Property(e => e.Permissions)
                    .HasColumnType("tinyint(10)")
                    .HasColumnName("permissions");

                entity.Property(e => e.RememberToken)
                    .HasMaxLength(255)
                    .HasColumnName("remember_token");

                entity.Property(e => e.TempPassword)
                    .HasMaxLength(1)
                    .HasColumnName("temp_password")
                    .HasDefaultValueSql("'N'")
                    .IsFixedLength();

                entity.Property(e => e.UpdatedAt)
                    .HasColumnType("timestamp")
                    .HasColumnName("updated_at");

                entity.Property(e => e.LastVerificationEmailSentAt)
                    .HasColumnType("timestamp")
                    .HasColumnName("last_verification_email_sent_at");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("username");

                entity.Property(e => e.EmailConfirmed)
                    .HasColumnType("tinyint(10)")
                    .HasColumnName("email_confirmed");

                entity.Property(e => e.ResetPasswordToken)
                    .HasColumnType("longtext")
                    .HasColumnName("reset_password_token");

                entity.Property(e => e.ResetPasswordExpiration)
                    .HasColumnType("datetime")
                    .HasColumnName("reset_password_expiration");
            });

            modelBuilder.Entity<Vote>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.CommId, e.MatchId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.ToTable("votes");

                entity.HasIndex(e => e.MatchId, "FK_votes_matches");

                entity.HasIndex(e => e.CommId, "votes_comm_FK");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .HasColumnName("account_id");

                entity.Property(e => e.CommId)
                    .HasColumnType("int(11)")
                    .HasColumnName("comm_id");

                entity.Property(e => e.MatchId)
                    .HasColumnType("int(11)")
                    .HasColumnName("match_id");

                entity.Property(e => e.Reason)
                    .HasColumnType("text")
                    .HasColumnName("reason");

                entity.Property(e => e.Vote1)
                    .HasColumnType("int(11)")
                    .HasColumnName("vote");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.VoteAccounts)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("votes_account_FK");

                entity.HasOne(d => d.Comm)
                    .WithMany(p => p.VoteComms)
                    .HasForeignKey(d => d.CommId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("votes_comm_FK");

                entity.HasOne(d => d.Match)
                    .WithMany(p => p.VoteMatches)
                    .HasForeignKey(d => d.MatchId)
                    .HasConstraintName("FK_votes_matches");
            });

            modelBuilder.Entity<EmailConfirmationToken>(entity =>
            {
                entity.ToTable("email_confirmation_tokens");

                entity.Property(e => e.Id)
                    .HasColumnType("int")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasMaxLength(256)
                    .HasColumnName("token");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasColumnType("int(11)")
                    .HasColumnName("user_id");

                entity.Property(e => e.CreatedAt)
                    .IsRequired()
                    .HasColumnType("timestamp")
                    .HasColumnName("created_at");

                entity.Property(e => e.ExpirationDate)
                    .IsRequired()
                    .HasColumnType("timestamp")
                    .HasColumnName("expiration_date");

                entity.HasIndex(e => e.UserId).HasDatabaseName("FK_email_confirmation_tokens_users");
            });

            modelBuilder.Entity<RefreshToken>(entity =>
            {
                entity.ToTable("refresh_tokens");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Token).HasColumnName("token");

                entity.Property(e => e.ExpirationDate).HasColumnName("expiration_date");

                entity.Property(e => e.CreatedAt).HasColumnName("created_at");

                entity.Property(e => e.Revoked).HasColumnName("revoked");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.RefreshTokens)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_refresh_tokens_users");
            });

            modelBuilder.Entity<StoredItem>(entity =>
            {
                entity.ToTable("stored_items");

                entity.HasIndex(e => e.StashId, "stored_items_stash_FK");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.StashId)
                    .HasColumnType("int(11)")
                    .HasColumnName("stash_id");

                entity.Property(e => e.CreationDate).HasColumnName("creation_date");

                entity.Property(e => e.Type)
                    .HasColumnType("int(11)")
                    .HasColumnName("type");

                entity.HasOne(d => d.UserStash)
                    .WithMany(p => p.StoredItems)
                    .HasForeignKey(d => d.StashId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("stored_items_stash_FK");
            });

            modelBuilder.Entity<UserStash>(entity =>
            {
                entity.ToTable("user_stashes");

                entity.HasIndex(e => e.AccountId, "user_stash_account_FK");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.AccountId)
                    .HasColumnType("int(11)")
                    .HasColumnName("account_id");

                entity.Property(e => e.Gold)
                    .HasColumnType("int(11)")
                    .HasColumnName("gold");

                entity.HasOne(d => d.Account)
                    .WithOne(p => p.UserStash)
                    .HasForeignKey<UserStash>(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_stash_account_FK");
            });


            modelBuilder.Entity<Release>(entity =>
            {
                entity.ToTable("releases");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasColumnName("name");

                entity.Property(e => e.TagName)
                    .HasColumnName("tag_name");

                entity.Property(e => e.Description)
                    .HasColumnName("description");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at");

                entity.Property(e => e.PublishedAt)
                    .HasColumnName("published_at");

                entity.Property(e => e.IsDraft)
                    .HasColumnName("is_draft");

                entity.Property(e => e.IsPrerelease)
                    .HasColumnName("is_prerelease");
            });

            modelBuilder.Entity<Asset>(entity =>
            {
                entity.ToTable("assets");

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasColumnName("name");

                entity.Property(e => e.ContentType)
                    .HasColumnName("content_type");

                entity.Property(e => e.Size)
                    .HasColumnName("size");

                entity.Property(e => e.DownloadUrl)
                    .HasColumnName("download_url");

                entity.Property(e => e.ReleaseId)
                    .HasColumnName("release_id");

                entity.HasOne(e => e.Release)
                    .WithMany(r => r.Assets)
                    .HasForeignKey(e => e.ReleaseId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_asset_release");
            });

            modelBuilder.Entity<LauncherUpdate>(entity =>
            {
                entity.ToTable("launcher_updates");

                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Version).HasColumnName("version");
                entity.Property(e => e.Notes).HasColumnName("notes");
                entity.Property(e => e.PubDate).HasColumnName("pub_date");

                entity.Property(e => e.PlatformsJson)
                    .HasColumnName("platforms_json")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<LauncherArticle>(entity =>
            {
                entity.ToTable("launcher_articles");

                entity.HasKey(e => e.Id);

                entity.Property(e => e.Md)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Thumb)
                    .HasMaxLength(255);

                entity.Property(e => e.Release)
                    .IsRequired();

                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Authors)
                    .HasConversion(
                        v => string.Join(',', v),
                        v => v.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList()
                    )
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<LauncherArticleContent>(entity =>
            {
                entity.ToTable("launcher_article_content");

                entity.HasKey(e => e.Id);

                entity.Property(e => e.Md)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Content)
                    .IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
