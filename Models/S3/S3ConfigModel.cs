﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S2Api.Models.S3
{
    public class S3ConfigModel
    {
        public string Endpoint { get; set; }
        public string Region { get; set; }
    }
}
