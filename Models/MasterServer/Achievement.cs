﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Achievement
    {
        public Achievement()
        {
            Badges = new HashSet<Badge>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Style { get; set; }
        public int? Points { get; set; }
        public string Condition { get; set; }
        public int? Value { get; set; }

        public virtual ICollection<Badge> Badges { get; set; }
    }
}
