﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Commander
    {
        public int AccountId { get; set; }
        public int MatchId { get; set; }
        public int TeamId { get; set; }
        public int Builds { get; set; }
        public int Exp { get; set; }
        public int Gold { get; set; }
        public int Razed { get; set; }
        public int HpHealed { get; set; }
        public int Pdmg { get; set; }
        public int Kills { get; set; }
        public int Debuffs { get; set; }
        public int Buffs { get; set; }
        public int Orders { get; set; }
        public int Secs { get; set; }
        public string Ip { get; set; }
        public int EndStatus { get; set; }
        public int? Sf { get; set; }

        public virtual User Account { get; set; }
        public virtual Match Match { get; set; }
        public virtual Team Team { get; set; }
    }
}
