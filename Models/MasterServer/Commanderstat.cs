﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Commanderstat
    {
        public int AccountId { get; set; }
        public int? Wins { get; set; }
        public int? Losses { get; set; }
        public int? DConns { get; set; }
        public int? Exp { get; set; }
        public int? EarnedExp { get; set; }
        public int? Builds { get; set; }
        public int? Gold { get; set; }
        public int? Razed { get; set; }
        public int? HpHealed { get; set; }
        public int? Pdmg { get; set; }
        public int? Kills { get; set; }
        public int? Assists { get; set; }
        public int? Debuffs { get; set; }
        public int? Buffs { get; set; }
        public int? Orders { get; set; }
        public int? Secs { get; set; }
        public int? Winstreak { get; set; }
    }
}
