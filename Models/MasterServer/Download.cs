﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Download
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public int? CategorieId { get; set; }
        public int? ImageId { get; set; }

        public virtual Category Categorie { get; set; }
        public virtual Image Image { get; set; }
    }
}
