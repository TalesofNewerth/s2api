﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Item
    {
        public int AccountId { get; set; }
        public int? Type { get; set; }
        public DateOnly? ExpDate { get; set; }
        public int Id { get; set; }

        public virtual User Account { get; set; }
    }
}
