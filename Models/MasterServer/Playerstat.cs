﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Playerstat
    {
        public int AccountId { get; set; }
        public int? Exp { get; set; }
        public int? Wins { get; set; }
        public int? DConns { get; set; }
        public int? Losses { get; set; }
        public int? Ties { get; set; }
        public int? Kills { get; set; }
        public int? Deaths { get; set; }
        public int? Assists { get; set; }
        public int? Souls { get; set; }
        public int? Razed { get; set; }
        public int? Pdmg { get; set; }
        public int? Bdmg { get; set; }
        public int? Npc { get; set; }
        public int? HpHealed { get; set; }
        public int? Res { get; set; }
        public int? HpRepaired { get; set; }
        public int? Gold { get; set; }
        public int? Secs { get; set; }
        public int? Malphas { get; set; }
        public int? Revenant { get; set; }
        public int? Devourer { get; set; }
    }
}
