﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Match
    {
        public Match()
        {
            Actionplayers = new HashSet<Actionplayer>();
            Commanders = new HashSet<Commander>();
        }

        public int Id { get; set; }
        public int? Winner { get; set; }
        public TimeOnly Duration { get; set; }
        public string Map { get; set; }

        public virtual ICollection<Actionplayer> Actionplayers { get; set; }
        public virtual ICollection<Commander> Commanders { get; set; }
    }
}
