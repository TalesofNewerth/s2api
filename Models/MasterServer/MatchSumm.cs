﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class MatchSumm
    {
        public MatchSumm()
        {
            Karmas = new HashSet<Karma>();
        }

        public int Id { get; set; }
        public int Port { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Map { get; set; }
        public int ServerId { get; set; }

        public virtual ICollection<Karma> Karmas { get; set; }
    }
}
