using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class UserStash
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int Gold { get; set; }

        public virtual User Account { get; set; }
        public virtual ICollection<StoredItem> StoredItems { get; set; }
    }
}
