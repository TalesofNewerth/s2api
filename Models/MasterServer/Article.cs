﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? AccountId { get; set; }
        public int? ImageId { get; set; }

        public virtual User Account { get; set; }
    }
}
