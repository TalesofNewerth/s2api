﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Team
    {
        public Team()
        {
            Actionplayers = new HashSet<Actionplayer>();
            Commanders = new HashSet<Commander>();
        }

        public int Id { get; set; }
        public int Match { get; set; }
        public string Race { get; set; }
        public int? AvgSf { get; set; }
        public string Commander { get; set; }

        public virtual ICollection<Actionplayer> Actionplayers { get; set; }
        public virtual ICollection<Commander> Commanders { get; set; }
    }
}
