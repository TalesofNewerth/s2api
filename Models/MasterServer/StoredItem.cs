using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class StoredItem
    {
        public int? StashId { get; set; }
        public int? Type { get; set; }
        public DateTime? CreationDate { get; set; }
        public int Id { get; set; }
        public virtual UserStash UserStash { get; set; }
    }
}
