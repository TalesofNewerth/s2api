﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Playerinfo
    {
        public int AccountId { get; set; }
        public int? OverallR { get; set; }
        public int? Sf { get; set; }
        public int? Lf { get; set; }
        public int? Level { get; set; }
        public int? ClanId { get; set; }
        public int? Karma { get; set; }
        public int? Ap { get; set; }

        public virtual User Account { get; set; }
    }
}
