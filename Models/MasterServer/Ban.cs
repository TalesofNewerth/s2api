﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Ban
    {
        public int AccountId { get; set; }
        public DateTime? Banneduntil { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int Id { get; set; }

        public virtual User Account { get; set; }
    }
}
