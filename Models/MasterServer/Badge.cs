﻿using System;
using System.Collections.Generic;

namespace S2Api.Models.MasterServer
{
    public partial class Badge
    {
        public int AccountId { get; set; }
        public int AchievementId { get; set; }
        public DateTime? CreatedAt { get; set; }

        public virtual User Account { get; set; }
        public virtual Achievement Achievement { get; set; }
    }
}
