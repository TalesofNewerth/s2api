using System;
using System.Collections.Generic;

namespace S2Api.Models.Launcher
{
    public class LauncherArticle
    {
        public int Id { get; set; }
        public string Md { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string Thumb { get; set; }
        public List<string> Authors { get; set; }
        public DateTime Release { get; set; }
        public string Category { get; set; }
    }

    public class LauncherArticleContent
    {
        public int Id { get; set; }
        public string Md { get; set; }
        public string Content { get; set; }
    }
}
