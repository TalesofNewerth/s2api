using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace S2Api.Models.Launcher
{
    public class LauncherUpdate
    {
        

        public int Id { get; set; }
        public string Version { get; set; }
        public string Notes { get; set; }

        private DateTime _pubDate;
        
        [JsonPropertyName("pub_date")]
        public DateTime PubDate
        {
            get => _pubDate == DateTime.MinValue ? DateTime.UtcNow : _pubDate;
            set => _pubDate = value;
        }
        
        public string PlatformsJson { get; set; }

        [NotMapped]
        public Dictionary<string, PlatformInfo> Platforms
        {
            get => string.IsNullOrEmpty(PlatformsJson) ? null : JsonSerializer.Deserialize<Dictionary<string, PlatformInfo>>(PlatformsJson);
            set => PlatformsJson = JsonSerializer.Serialize(value);
        }
    }

    public class PlatformInfo
    {
        public string Signature { get; set; }
        public string Url { get; set; }
    }
}