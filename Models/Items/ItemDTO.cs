using System;

namespace S2Api.Models.Items;

public class ItemDTO
{
    public ItemDTO()
    {
        
    }
    
    public int Id { get; set; }
    public int AccountId { get; set; }
    public int Type { get; set; }
    public DateTime ExpDate { get; set; }
}