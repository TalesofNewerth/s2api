using System;
using System.Collections.Generic;
using System.Linq;
using S2Api.Enums;
using S2Api.Models.MasterServer;

namespace S2Api.Models.Items;

public class Item
{
    private const int BaseCost = 100;
    private const int Points = 15;
    //public so they get serialized.
    public RuneTypeEnum Type { get; set; }
    public RuneColorEnum Color { get; set; }
    public RunePassiveEnum Passive { get; set; }
    public RuneActiveEnum Active { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime ExpDate { get; set; }
    
    public int Id { get; set; }

    private List<ItemConfiguration> Items { get; }
    private RuneTypeEnum[] AllowedRuneTypes 
    { 
        get { return Items.Where(x => x.Type == (int)RuneStageEnum.Type).Select(x => (RuneTypeEnum)x.Id).ToArray(); } 
    }
    
    private RuneColorEnum[] AllowedRuneColors
    {
        get { return Items.Where(x => x.Type == (int)RuneStageEnum.Color).Select(x => (RuneColorEnum)x.Id).ToArray(); }
    }

    private RunePassiveEnum[] AllowedRunePassives
    {
        get
        {
            return Items.Where(x => x.Type == (int)RuneStageEnum.Passive).Select(x => (RunePassiveEnum)x.Id).ToArray();
        }
    }

    private RuneActiveEnum[] AllowedRuneActives
    {
        get { return Items.Where(x => x.Type == (int)RuneStageEnum.Active).Select(x => (RuneActiveEnum)x.Id).ToArray(); }
    }
    
    private bool IsRandom { get; }
    
    public Item(List<ItemConfiguration> items)
    {
        Items = items;
        IsRandom = true;
        GenerateRandomItem();
    }
    
    public Item(List<ItemConfiguration> items, RuneTypeEnum type, RuneColorEnum color, RunePassiveEnum passive, RuneActiveEnum active)
    {
        Type = type;
        Color = color;
        Passive = passive;
        Active = active;
        Items = items;
    }

    public Item(List<ItemConfiguration> items, int type)
    {
        Items = items;
        var digits = type.ToString().ToCharArray().Select(x => int.Parse(x.ToString())).ToList();
        if (digits.Count != 4)
        {
            throw new ArgumentException("The type needs to be 4 digits");
        }
        
        Type = (RuneTypeEnum)digits[0];
        Color = (RuneColorEnum)digits[1];
        Passive = (RunePassiveEnum)digits[2];
        Active = (RuneActiveEnum)digits[3];
    }
    
    public Item(List<ItemConfiguration> items, StoredItem item) : this(items, item.Type.GetValueOrDefault(0))
    {
        CreationDate = item.CreationDate.GetValueOrDefault();
        Id = item.Id;
    }
    
    public Item(List<ItemConfiguration> items, S2Api.Models.MasterServer.Item item) : this(items, item.Type.GetValueOrDefault(0))
    {
        ExpDate = item.ExpDate.GetValueOrDefault().ToDateTime(TimeOnly.MinValue);
        Id = item.Id;
    }

    /// <summary>
    /// Validates whether the current rune configuration is valid based on its type, color, passive, and active properties.
    /// </summary>
    /// <returns>True if the rune configuration is valid; otherwise, false.</returns>
    public bool ValidateRune()
    {
        return ValidateNumber() && ValidatePointCost();
    }
    
    /// <summary>
    /// Calculates a unique number for an item based on its type, color, passive, and active properties.
    /// </summary>
    /// <returns>A unique number representing the item.</returns>
    public int GetNumber()
    {
        return (int)Type * 1000 + (int)Color * 100 + (int)Passive * 10 + (int)Active;
    }

    /// <summary>
    /// Calculates the gold cost of an item.
    /// </summary>
    /// <returns>The gold cost of the item.</returns>
    public int GetGoldCost()
    {
        return IsRandom ? (int)(BaseCost * 0.50) : BaseCost;
    }

    /// <summary>
    /// Calculates the salvaged value of an item.
    /// </summary>
    /// <returns>The salvaged value of the item.</returns>
    public static int GetSalvagedValue()
    {
        return (int)(BaseCost * 0.40);
    }

    /// <summary>
    /// Generates a random item by assigning random values to its type, color, passive, and active properties.
    /// </summary>
    private void GenerateRandomItem()
    {
        var random = new Random();
        int remainingPoints = Points;

        Type = GenerateRune(AllowedRuneTypes, RuneStageEnum.Type, remainingPoints, random);
        remainingPoints -= GetCost(Type, RuneStageEnum.Type);

        Color = GenerateRune(AllowedRuneColors, RuneStageEnum.Color, remainingPoints, random);
        remainingPoints -= GetCost(Color, RuneStageEnum.Color);

        Passive = GenerateRune(AllowedRunePassives, RuneStageEnum.Passive, remainingPoints, random);
        remainingPoints -= GetCost(Passive, RuneStageEnum.Passive);

        Active = GenerateRune(AllowedRuneActives, RuneStageEnum.Active, remainingPoints, random);
    }

    /// <summary>
    /// Validates the point cost of an item based on its type, color, passive, and active properties.
    /// </summary>
    /// <returns>True if the point cost is valid, false otherwise.</returns>
    private bool ValidatePointCost()
    {
        var type = GetCost(Type, RuneStageEnum.Type);
        var color = GetCost(Color, RuneStageEnum.Color);
        var passive = GetCost(Passive, RuneStageEnum.Passive);
        var active = GetCost(Active, RuneStageEnum.Active);
        
        return type + color + passive + active <= Points;
    }

    private int GetCost<T>(T id, RuneStageEnum type) where T : Enum
    {
        return Items.Where(x => x.Id == Convert.ToInt32(id) && x.Type == (int)type).Select(x => x.Cost)
            .FirstOrDefault();
    }

    /// <summary>
    /// Generates a random rune from the given array of allowed runes, based on the specified stage, remaining points, and a random number generator.
    /// </summary>
    /// <typeparam name="T">The type of the rune, which must be an enumeration.</typeparam>
    /// <param name="allowedRunes">The array of allowed runes to choose from.</param>
    /// <param name="stage">The stage of the rune to generate.</param>
    /// <param name="remainingPoints">The remaining points available to spend on runes.</param>
    /// <param name="random">The random number generator to use.</param>
    /// <returns>A random rune from the allowed runes that satisfies the given constraints.</returns>
    private T GenerateRune<T>(T[] allowedRunes, RuneStageEnum stage, int remainingPoints, Random random) where T : Enum
    {
        T chosenRune;

        do
        {
            chosenRune = allowedRunes[random.Next(allowedRunes.Length)];
        } while (GetCost(chosenRune, stage) > remainingPoints);

        return chosenRune;
    }

    /// <summary>
    /// Validates whether the current item configuration is valid based on its type, color, passive, and active properties.
    /// </summary>
    /// <returns>True if the item configuration is valid; otherwise, false.</returns>
    private bool ValidateNumber()
    {
        return AllowedRuneTypes.Contains(Type) && AllowedRuneColors.Contains(Color) &&
               AllowedRunePassives.Contains(Passive) && AllowedRuneActives.Contains(Active);
    }
}