namespace S2Api.Models.Items;

public class ItemConfiguration
{
    public int Id { get; set; }
    public int Type { get; set; }
    public string Text { get; set; }
    public string Status { get; set; }
    public string Image { get; set; }
    public string AffixImage { get; set; }
    public int Cost { get; set; }
    public string Description { get; set; }
}