﻿using PhpSerializerNET;
using System.Text.Json.Serialization;

namespace S2Api.Models.Stats
{
    public class CommanderDTO
    {
        [PhpProperty("c_builds")]
        public int Builds { get; set; }
        [PhpProperty("c_exp")]
        public int Exp { get; set; }
        [PhpProperty("c_gold")]
        public int Gold { get; set; }
        [PhpProperty("c_razed")]
        public int Razed { get; set; }
        [PhpProperty("c_hp_healed")]
        public int HpHealed { get; set; }
        [PhpProperty("c_pdmg")]
        public int PDmg { get; set; }
        [PhpProperty("c_kills")]
        public int Kills { get; set; }
        [PhpProperty("c_debuffs")]
        public int Debuffs { get; set; }
        [PhpProperty("c_buffs")]
        public int Buffs { get; set; }
        [PhpProperty("c_orders")]
        public int Orders { get; set; }
        [PhpProperty("c_secs")]
        public int Secs { get; set; }
        [PhpProperty("c_end_status")]
        public int EndStatus { get; set; }
        [PhpProperty("sf")]
        public int Sf { get; set; }
        [PhpProperty("ip")]
        public string Ip { get; set; }
        [PhpProperty("account_id")]
        public int AccountId { get; set; }
        [PhpProperty("c_team")]
        public int Team { get; set; }
        [PhpProperty("clan_name")]
        public string ClanName { get; set; }
        [PhpProperty("c_auto_win")]
        public int CAutoWin { get; set; }
        [PhpProperty("rec_stats")]
        public int RecStats { get; set; }
    }
}
