﻿using PhpSerializerNET;
using System.Text.Json.Serialization;

namespace S2Api.Models.Stats
{
    public class ActionPlayerDTO
    {
        [PhpProperty("exp")]
        public int Exp { get; set; }
        [PhpProperty("kills")]
        public int Kills { get; set; }
        [PhpProperty("deaths")]
        public int Deaths { get; set; }
        [PhpProperty("assists")]
        public int Assists { get; set; }
        [PhpProperty("souls")]
        public int Souls { get; set; }
        [PhpProperty("razed")]
        public int Razed { get; set; }
        [PhpProperty("pdmg")]
        public int Pdmg { get; set; }
        [PhpProperty("bdmg")]
        public int Bdmg { get; set; }
        [PhpProperty("hp_healed")]
        public int HpHealed { get; set; }
        [PhpProperty("res")]
        public int Res { get; set; }
        [PhpProperty("gold")]
        public int Gold { get; set; }
        [PhpProperty("hp_repaired")]
        public int HpRepaired { get; set; }
        [PhpProperty("secs")]
        public int Secs { get; set; }
        [PhpProperty("end_status")]
        public int EndStatus { get; set; }
        [PhpProperty("sf")]
        public int Sf { get; set; }
        [PhpProperty("ip")]
        public string Ip { get; set; }
        [PhpProperty("account_id")]
        public int AccountId { get; set; }
        [PhpProperty("npc")]
        public int Npc { get; set; }
        [PhpProperty("team")]
        public int Team { get; set; }
        [PhpProperty("clan_name")]
        public string ClanName { get; set; }
        [PhpProperty("karma")]
        public int Karma { get; set; }
        [PhpProperty("auto_win")]
        public int AutoWin { get; set; }
        [PhpProperty("rec_stats")]
        public int RecStats { get; set; }
        [PhpProperty("malphas")]
        public int Malphas { get; set; }
        [PhpProperty("revenant")]
        public int Revenant { get; set; }
        [PhpProperty("devourer")]
        public int Devourer { get; set; }
    }
}
