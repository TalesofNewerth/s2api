﻿using S2Api.Models.MasterServer;

namespace S2Api.Models.Stats;

public class ParticipantDTO
{
    public int AccountId { get; set; }
    public int Secs { get; set; }
    public int TeamId { get; set; }
    public int EndStatus { get; set; }
    public int Exp { get; set; }
    public int Kills { get; set; }
    public int Assists { get; set; }
    public int HpHealed { get; set; }
    public int Pdmg { get; set; }
    public int Bdmg { get; set; }
    public int HpRepaired { get; set; }
    public bool IsCommander { get; set; }
    
    public static implicit operator ParticipantDTO(Actionplayer player)
    {
        return new ParticipantDTO
        {
            AccountId = player.AccountId,
            Secs = player.Secs,
            TeamId = player.TeamId,
            EndStatus = player.EndStatus,
            Exp = player.Exp,
            Kills = player.Kills,
            Assists = player.Assists,
            HpHealed = player.HpHealed,
            Pdmg = player.Pdmg,
            Bdmg = player.Bdmg,
            HpRepaired = player.HpRepaired,
            IsCommander = false
        };
    }
    
    public static implicit operator ParticipantDTO(Commander commander)
    {
        return new ParticipantDTO
        {
            AccountId = commander.AccountId,
            Secs = commander.Secs,
            TeamId = commander.TeamId,
            EndStatus = commander.EndStatus,
            Exp = commander.Exp,
            Kills = commander.Kills,
            Assists = 0,
            HpHealed = commander.HpHealed,
            Pdmg = commander.Pdmg,
            Bdmg = 0,
            HpRepaired = 0,
            IsCommander = true
        };
    }
    
}