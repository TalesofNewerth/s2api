﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace S2Api.Models.Players
{
    public class PlayerModel
    {
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int SF { get; set; }
        public int Level { get; set; }
        public string Clan { get; set; }
        public int Karma { get; set; }
        public int AchievementPoints { get; set; }
        public DateTime? AccountCreated { get; set; }
        public DateTime? LastLoggedIn { get; set; }
    }
}
